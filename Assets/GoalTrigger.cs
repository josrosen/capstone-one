using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalTrigger : MonoBehaviour
{
    public string nextRoom;
    public AudioClip victorySound;
    public ParticleSystem confetti;
    public ParticleSystem secondaryConfetti;
    bool startConfetti = false;
    float timer;

    public void beginConfetti()
    {
        startConfetti = true;
        FindObjectOfType<SoundPlayer>().InvokePrioritySound(victorySound);
        confetti.gameObject.SetActive(true);
        timer = 2;
    }

    private void Update()
    {
        if(startConfetti)
        {
            timer -= Time.deltaTime;
            if(timer <= 0 || confetti.isStopped)
            {
                secondaryConfetti.gameObject.SetActive(true);
                FindObjectOfType<PlayerMovement>().playState = "lvClear";
                startConfetti = false;
            }
        }
    }

}
