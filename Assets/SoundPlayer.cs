using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundPlayer : MonoBehaviour
{
    bool begin;
    bool fadeOut;
    float playDelay;
    public AudioClip previewModeBGM;
    public AudioClip inGameBGM;
    public AudioClip victory;
    public AudioSource bgm;
    public AudioSource[] sounds;
    public AudioSource prioritySound;
    public AudioClip currSound;
    public AudioClip[] queue;
    int qIndex;
    int qSize;
    double startTime;

    //Time management
    bool newBeat = false;
    int currSixteenth;

    GameController gameCon;
    bool trackerFound = true;
    ScoreTracker tracker;

    // Start is called before the first frame update
    void Start()
    {
        begin = false;
        fadeOut = false;
        queue = new AudioClip[4];
        qIndex = -1;
        qSize = 0;
        gameCon = FindObjectOfType<GameController>();
        currSixteenth = 0;
        bgm.clip = previewModeBGM;
        bgm.Play();
        tracker = FindObjectOfType<ScoreTracker>();
        if (tracker)
        {
            bgm.volume = tracker.GetMusVolume();
        }
        else
            trackerFound = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(!trackerFound)
        {
            tracker = FindObjectOfType<ScoreTracker>();
            if (tracker)
            {
                trackerFound = true;
                bgm.volume = tracker.GetMusVolume();
            }
            else
                trackerFound = false;
        }
        //Audio will be uniformly played from the same area
        transform.position = FindObjectOfType<PlayerMovement>().transform.position;

        //Update note profiling from gameCon
        if (currSixteenth != gameCon.GetBeatRemainingSixteenth())
        {
            currSixteenth = (int)gameCon.GetBeatRemainingSixteenth();
            //if(currSixteenth == 0 || currSixteenth == 2 || currSixteenth == 4 || currSixteenth == 6 )
            if(currSixteenth % 1 == 0)
                newBeat = true;
        }
        else
        {
            newBeat = false;
        }
        if(begin)
        {
            if(newBeat)
            {
                /*if (currEighth % 2 == 0) //background taps (will be replaced by BGM)
                {
                    bgm.Play();
                }*/

                //playerSounds
                if (qSize >= 0)
                {
                    AudioClip play1 = null;
                    AudioClip play2 = null;
                    AudioClip play3 = null;

                    for (int i = 0; i < sounds.Length; i++)
                    {
                        if (!sounds[i].isPlaying)
                        {
                            //find next sound in queue sequentially (prevents loss of sounds or array out of bounds)
                            for (int j = 0; j < queue.Length; j++)
                            {
                                if (queue[j] != null)
                                {
                                    //Check for critical hit
                                    if (currSixteenth % 2 == 0 || currSixteenth == 3)
                                        sounds[i].pitch = 1;
                                    else
                                        sounds[i].pitch = 0.9f;
                                    sounds[i].clip = queue[j];
                                    //store sound to catch 
                                    if (!play1)
                                        play1 = sounds[i].clip;
                                    else if (!play2)
                                        play2 = sounds[i].clip;
                                    else if (!play3)
                                        play3 = sounds[i].clip;
                                    else
                                        break;
                                    queue[j] = null;
                                    qSize--;
                                    sounds[i].Play();
                                }
                            }
                        }
                        /*else if(sounds[i].time < sounds[i].clip.length/10) //fill our play variables with data of current sounds
                        {
                            if (!play1)
                                play1 = sounds[i].clip;
                            else if (!play2)
                                play2 = sounds[i].clip;
                            else if (!play3)
                                play3 = sounds[i].clip;
                            else
                                break;
                        }*/
                    }
                    
                }
            }
        }
        if(fadeOut)
        {
            bgm.volume -= 0.05f;
            if (bgm.volume <= 0)
                bgm.Stop();
            for (int i = 0; i < sounds.Length; i++)
            {
                if (sounds[i].isPlaying)
                    sounds[i].volume -= 0.05f;
            }
        }
    }

    public void InitiateSong(double soundStart, double delay)
    {
        begin = true;
        bgm.clip = inGameBGM;
        startTime = soundStart;
        playDelay = (float)delay;
        bgm.PlayDelayed((float)delay); //delay by one 16th note
    }

    public void SetBGMVolume()
    {
        bgm.volume = tracker.GetMusVolume();
    }

    public void StopBGM()
    {
        bgm.Stop();
    }

    public void InvokePlayerSound(AudioClip sound)
    {
        //We are only looking for on-beat movement sounds
        qSize++;
        qIndex++;
        if (qIndex >= queue.Length)
            qIndex = 0;
        queue[qIndex] = sound;
    }

    public void InvokePrioritySound(AudioClip sound)
    {
        //stop sounds
        begin = false;
        //fade out other sounds
        fadeOut = true;
       
        //play priority sound
        prioritySound.clip = sound;
        prioritySound.Play();
    }

    public void PlayVictoryTune()
    {
        bgm.clip = victory;
        bgm.loop = false;
        fadeOut = false;
        bgm.volume = tracker.GetMusVolume();
        bgm.Play();
    }
}
