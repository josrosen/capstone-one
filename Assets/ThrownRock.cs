using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrownRock : MonoBehaviour
{
    public float movSpeed = 0;
    public int movDir;
    public bool readyToContinue = false;
    float totalDist = 0;
    float timer = 0;

    public AudioSource collisionSound;
    bool soundPlayed = false;
    public ParticleSystem particles;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        //play a hit sound then destruct
        readyToContinue = true;
        soundPlayed = true;
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<CircleCollider2D>().enabled = false;
        totalDist = 2;
        collisionSound.Play();

        //Create a particle system with colors that indicate a hit
        ParticleSystem.MainModule main = particles.main;
        main.startColor = new Color(183 / 255f, 148 / 255f, 101 / 255f); //#B79465
        ParticleSystem rockDust = Instantiate(particles);
        rockDust.transform.position = transform.position;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //play a hit sound then destruct
        if (collision.tag == "BasicCollision" || collision.tag == "SnapTrap" || totalDist > 1.2 )
        {
            readyToContinue = true;
            soundPlayed = true;
            GetComponent<SpriteRenderer>().enabled = false;
            GetComponent<CircleCollider2D>().enabled = false;
            totalDist = 2;
            if (collision.tag != "SnapTrap" && collision.tag != "Pitfall") collisionSound.Play();
        }
    }

    private void Start()
    {
        movSpeed = 0;
        movDir = 0;
        foreach(AudioSource sfx in FindObjectsOfType<AudioSource>())
            sfx.volume = FindObjectOfType<ScoreTracker>().GetMusVolume();
    }

    // Update is called once per frame
    void Update()
    {
        //movement
        if (!soundPlayed)
        {
            float dist = movSpeed * Time.deltaTime;
            totalDist += dist;
            switch (movDir)
            {
                case 0: //up
                    {
                        transform.Translate(new Vector3(0, movSpeed * Time.deltaTime, 0));
                        break;
                    }
                case 1: //down
                    {
                        transform.Translate(new Vector3(0, -movSpeed * Time.deltaTime, 0));
                        break;
                    }
                case 2: //left
                    {
                        transform.Translate(new Vector3(-movSpeed * Time.deltaTime, 0, 0));
                        break;
                    }
                case 3: //right
                    {
                        transform.Translate(new Vector3(movSpeed * Time.deltaTime, 0, 0));
                        break;
                    }
            }

            if (!readyToContinue && totalDist >= 1.5)
                timer = (movDir / 2) - totalDist;
            if (totalDist >= 2)
            {
                soundPlayed = true;
                collisionSound.pitch = 0.42f;
                collisionSound.Play();
                GetComponent<SpriteRenderer>().enabled = false;
                GetComponent<CircleCollider2D>().enabled = false;

                //Create a particle system with colors that indicate a hit
                ParticleSystem.MainModule main = particles.main;
                main.startColor = new Color(183/255f, 148/255f, 101/255f); //#B79465
                ParticleSystem rockDust = Instantiate(particles);
                rockDust.transform.position = transform.position;

            }
        }
        else if (timer > 0)
            timer -= movSpeed * Time.deltaTime;
        else
        {
            if (!readyToContinue)
                readyToContinue = true;
            if (!collisionSound.isPlaying)
                Destroy(gameObject);
        }
    }
}
