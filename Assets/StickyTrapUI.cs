using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickyTrapUI : MonoBehaviour
{
    PlayerMovement parent;
    int stuckCount;
    SpriteRenderer spr;
    public Sprite count2;
    public Sprite count1;
    // Start is called before the first frame update
    void Start()
    {
        parent = GetComponentInParent<PlayerMovement>();
        spr = GetComponent<SpriteRenderer>();
        stuckCount = parent.stickyTaps;
    }

    // Update is called once per frame
    void Update()
    {
        if(stuckCount != parent.stickyTaps)
        {
            stuckCount = parent.stickyTaps;
            switch(stuckCount)
            {
                case 0:
                    {
                        spr.sprite = null;
                        spr.enabled = false;
                        break;
                    }
                case 1:
                    {
                        spr.sprite = count1;
                        break;
                    }
                case 2:
                    {
                        spr.enabled = true;
                        spr.sprite = count2;
                        break;
                    }
            }
        }
    }
}
