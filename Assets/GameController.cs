using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class GameController : MonoBehaviour
{
    public GenerateUI currCanvas;
    public PlayerMovement playerObj;
    public AudioClip beatSound;
    public AudioSource soundPlayer;
    public AudioSource anthrSoundPlayer;
    public AudioSource oneMoreSoundPlayer;
    SoundPlayer sound;
    ScoreTracker tracker;
    bool trackerFound = true;

    //Rhythm variables
    bool levelStart = false;
    double levelStartTime = 0;
    public float BPM = 120;
    float BPS;
    public double beatLen;
    public double beatTimer = 0;
    public float delay;
    float configDelay;

    public int currBeatQtr = 0;
    public int currBeatEighth = 0;
    int currBeatSixteenth = 0;
    bool comboGuard = false;

    //Scene Manager
    Scene currScene;
    Scene nextScene;
    private IEnumerator sceneLoader;

    // Start is called before the first frame update
    void Start()
    {
        BPS = BPM / 60f;
        beatLen = 60f / BPM;
        playerObj = GameObject.FindObjectOfType<PlayerMovement>();
        currCanvas = FindObjectOfType<GenerateUI>();
        sound = FindObjectOfType<SoundPlayer>();

        //Assign volumes to all audio sources
        tracker = FindObjectOfType<ScoreTracker>();
        if (tracker)
        {
            foreach (AudioSource audio in FindObjectsOfType<AudioSource>(true))
            {
                audio.volume = tracker.GetSfxVolume();
            }
            configDelay = tracker.GetDelay();
            sound.SetBGMVolume();
        }
        else
            trackerFound = false;
    }

    

    // Update is called once per frame
    void Update()
    {
        //check beat progress (once level has started)
        if (levelStart)
        {
            //referenced https://www.gamedeveloper.com/audio/coding-to-the-beat---under-the-hood-of-a-rhythm-game-in-unity#close-modal
            float songPosition = (float)(AudioSettings.dspTime - levelStartTime);
            float songPositionInBeats = (float)(songPosition / beatLen);


            if (beatTimer < beatLen)
            {
                beatTimer += Time.deltaTime;
                if (!comboGuard)
                {
                    if (beatTimer > (beatLen * 0.6) && playerObj.canMove && playerObj.buffTime <= 0 && playerObj.playState == "normal")
                    {
                        ResetCombo();
                        //playerObj.canMove = false;
                    }
                }
                /*int temp = currBeatEighth;
                if (temp < GetBeatRemainingEighth())
                {
                    anthrSoundPlayer.Play();
                }
                temp = currBeatSixteenth;
                if (temp < GetBeatRemainingSixteenth())
                    oneMoreSoundPlayer.Play();*/
            }
            else if (playerObj.playState != "terminate" && playerObj.playState != "dead" && !(playerObj.playState == "lvClear" || playerObj.playState == "victoryAnim"))
            {
                beatTimer = (beatTimer - beatLen) + Time.deltaTime; //accounting for the extra difference from deltaTime
                //sound.InvokeGameConSound(beatSound);//soundPlayer.Play();
                //anthrSoundPlayer.Play();
                //oneMoreSoundPlayer.Play();
                currBeatQtr = currBeatQtr < 3 ? currBeatQtr + 1 : 0;
                currBeatEighth = 0;
                currBeatSixteenth = 0;
                if (playerObj.canMove == true && !comboGuard)
                    ResetCombo();
                else //if (!(currBeatQtr == 1 && Time.timeSinceLevelLoad - levelStartTime < 2))
                {
                    comboGuard = false;    
                    playerObj.canMove = true;
                }
            }

            if (playerObj)
            {
                if (playerObj.playState == "terminate")
                {
                    if (currCanvas.canvasState != "dead") currCanvas.SetCanvasUI("dead");

                }
                if (playerObj.playState == "lvClear" )
                {
                    if (currCanvas.canvasState != "lvClear")
                    {
                        sound.PlayVictoryTune();
                        EndCombo();
                        currCanvas.SetCanvasUI("lvClear");
                        currCanvas.comboCount = 0;
                    }
                }
                if(playerObj.playState == "victoryAnim")
                {

                }
            }
        }
        else //this should be used for the preview mode
        {
            if(playerObj.playState == "preview" && currCanvas.canvasState != "previewMode")
            {
                currCanvas.SetCanvasUI("previewMode");
            }
            if (playerObj.playState == "centeringCamera" && currCanvas.canvasState != "transition")
            {
                currCanvas.SetCanvasUI("transition");
            }
        }
    }

    public void StartLevel()
    {
        currCanvas.SetCanvasUI("activeGameplay");
        currCanvas.SetCanvasUI("comboCounterOff");

        

        levelStart = true;
        levelStartTime = AudioSettings.dspTime;

        //Start music obj and offset our beat timer
        delay = (float)beatLen / 4;
        EnableAtStart[] enabler = FindObjectsOfType<EnableAtStart>();
        for (int i = 0; i < enabler.Length; i++)
        {
            enabler[i].CallEnable(delay);
        }
        sound.InitiateSong(levelStartTime, delay+configDelay);
        
        //beatTimer -= delay;
    }

    public void AcceptMovement()
    {
        TimedLogic[] movingObjs = GameObject.FindObjectsOfType<TimedLogic>();
        for(int i = 0; i < movingObjs.Length; i++)
        {
            movingObjs[i].InvokeMovement(GetMoveSpeed());
        }
        PulseOnBeat[] pulsingObjs = GameObject.FindObjectsOfType<PulseOnBeat>();
        for (int i = 0; i < pulsingObjs.Length; i++)
        {
            pulsingObjs[i].SetPulse();
        }
    }

    public float GetMoveSpeed()
    {
        return (float)((2*BPS) / beatLen);
    }
    public float GetBeatRemainingQuarter()
    {
        //Debug.Log("Quarter = " + currBeatQtr + "/4");
        return currBeatQtr;
    }

    public int GetBeatRemainingEighth()
    {
        double timeMath = beatTimer + delay;
        currBeatEighth = (timeMath > beatLen / 2) ? 1 : 0;
        currBeatEighth += currBeatQtr * 2;
        //Debug.Log("Eighth = " + currBeatEighth + "/8");
        return currBeatEighth;
    }

    public float GetBeatRemainingSixteenth()
    {
        double timeMath = beatTimer + delay;
        double temp = beatLen / 4;
        if (timeMath < temp)
            currBeatSixteenth = 0;
        else if (timeMath > temp && timeMath < temp * 2)
            currBeatSixteenth = 1;
        else if (timeMath> temp * 2 && timeMath < temp * 3)
            currBeatSixteenth = 2;
        else currBeatSixteenth = 3;
        currBeatSixteenth += currBeatEighth * 2;
        //Debug.Log("Sixteenth = " + currBeatSixteenth + "/16");
        return currBeatSixteenth;
    }

    public void ComboPlus()
    {
        comboGuard = true;
        currCanvas.comboCount++;
    }

    public void ResetCombo()
    {
        //trackers for end of level
        if (playerObj.playState != "lvClear" && playerObj.playState != "victoryAnim")
        {
            currCanvas.totalPlayerMoves += currCanvas.comboCount;
            if (currCanvas.comboCount > currCanvas.highestPlayerCombo)
                currCanvas.highestPlayerCombo = currCanvas.comboCount;

            currCanvas.comboCount = 0;
        }
    }

    void EndCombo()
    {
        currCanvas.totalPlayerMoves += currCanvas.comboCount;
        if (currCanvas.comboCount > currCanvas.highestPlayerCombo)
            currCanvas.highestPlayerCombo = currCanvas.comboCount;

    }

    public void InsertItemUI(int itemID)
    {
        currCanvas.SetItemFrame(itemID);
    }

    public void ResetRoom()
    {
        sceneLoader = LoadingRoom(SceneManager.GetActiveScene().name);
        StartCoroutine(sceneLoader);
    }

    //Room Loading
    public void LoadRoom(string roomName)
    {

        /*if (FindObjectOfType<PlayerMovement>() != null)
            playerPos = FindObjectOfType<PlayerScript>().transform.position;*/
        //FindObjectOfType<EventSystem>().enabled = false;
        if (roomName != "0 Title")
        {
            if (roomName != "11 Win")
            {
                tracker.SetCurrentRoom(roomName);
                sceneLoader = LoadingRoom(roomName);
            }
            else
            {
                tracker.SetCurrentRoom("1 BasicMovement");
                sceneLoader = LoadingRoom(roomName);
            }
        }
        else
            sceneLoader = LoadingRoom(roomName);

        StartCoroutine(sceneLoader);
    }
    private IEnumerator LoadingRoom(string roomName)
    {
        //Send transition request to canvas then wait for finish
        yield return new WaitForSeconds(currCanvas.BeginExitTransition());


        //DontDestroyOnLoad(gameObject);
        //DontDestroyOnLoad(overworldUI);
        SceneManager.LoadScene(roomName, LoadSceneMode.Single);
        AsyncOperation nScene = SceneManager.LoadSceneAsync(roomName, LoadSceneMode.Single);
        nScene.allowSceneActivation = false;
        while (nScene.progress < 0.9f)
        {
            yield return null;
        }

        nScene.allowSceneActivation = true;

        while (!nScene.isDone)
        {
            yield return null;
        }

        

        nextScene = SceneManager.GetSceneByName(roomName);
        if (nextScene.IsValid() && currCanvas)
        {
            //SceneManager.MoveGameObjectToScene(gameObject, nextScene);
            SceneManager.SetActiveScene(nextScene);
        }
    }
}
