using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PulseOnBeat : MonoBehaviour
{
    SpriteRenderer spr;
    float pulseLen = 0.2f;
    float timer = 0;
    public float amount = 1;
    float size;
    Vector3 defaultScale;
    int sizeChange;

    // Start is called before the first frame update
    void Start()
    {
        spr = GetComponent<SpriteRenderer>();
        
        defaultScale = spr.transform.localScale;
        sizeChange = 0;
    }

    // Update is called once per frame
    void Update()
    {
        size = amount * Time.deltaTime;
        if (sizeChange == 1) //increasing
        {
            if (timer > 0)
            {
                if ((amount > 0 && spr.transform.localScale.x < defaultScale.x + amount) || (amount < 0 && spr.transform.localScale.x > defaultScale.x + amount))
                    spr.transform.localScale += new Vector3(size, size, 0);

                timer -= Time.deltaTime;
                
            }
            else
            {
                sizeChange = 2;
                timer = pulseLen;
            }
        }
        else if (sizeChange == 2) //decreasing
        {
            if (timer > 0)
            {
                if ((amount > 0 && spr.transform.localScale.x > defaultScale.x) || (amount < 0 && spr.transform.localScale.x < defaultScale.x))
                    spr.transform.localScale -= new Vector3(size, size, 0);
                timer -= Time.deltaTime;
                
            }
            else
            {
                sizeChange = 0;
                timer = 0;
                spr.transform.localScale = defaultScale;
            }
        }
    }

    public void SetPulse()
    {
        timer = pulseLen;
        sizeChange = 1;
    }
}
