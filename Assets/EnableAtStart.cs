using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableAtStart : MonoBehaviour
{
    Vector3 initTransform;
    public bool startDelayed = false;
    public bool reverse = false;
    bool beginDelay = false;
    float timer = 0;
    // Start is called before the first frame update
    void Start()
    {
        if (!reverse)
        {
            initTransform = transform.position;
            transform.position = new Vector3(1000000, 1000000, 0);
        }
    }

    private void Update()
    {
        if(startDelayed && beginDelay)
        {
            if(timer < 0)
            {
                transform.position = initTransform;
            }
            timer -= Time.deltaTime;
        }
    }

    public void CallEnable(float delay)
    {
        if (!startDelayed)
        {
            if (!reverse)
            {
                transform.position = initTransform;
            } 
            else
            {
                transform.position = new Vector3(100000, 1000000, 0);
            }
        }
        else
        {
            beginDelay = true;
            timer = delay;
        }
    }
}
