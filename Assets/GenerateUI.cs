using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class GenerateUI : MonoBehaviour
{
    public string canvasState;

    //active gameplay
    public Image itemImage;

    //combo
    public int comboCount;
    public TextMeshProUGUI comboTextMesh;
    private int comboIndex = -1;
    private Text comboText;
    bool pulse = true;
    float pulseTimer = 0;
    public float pulseLength = 0.2f;
    float defaultTextSize;
    float pulseToSize;

    //End-Level Stats
    public int levelPar = 100;            //plugged in per level (override)
    public TextMeshProUGUI endCombo;
    public TextMeshProUGUI highestCombo;
    public TextMeshProUGUI totalMoves;
    public TextMeshProUGUI levelComboPar;
    public TextMeshProUGUI gradeText;
    public int totalPlayerMoves;
    public int highestPlayerCombo;

    //storage
    public GameObject[] ListOfUI;
    public Sprite[] ItemSprites;

    //Transition animation
    public float transitionTime = 0.5f;
    public Animator transition;

    //Preview panel
    public Image previewPanel;
    public Image previewControl;
    public Image[] arrows;
    bool[] arrowStates = { true, true, true, true };
    float startAlpha;
    float currAlpha;
    bool fade = true;

    // Start is called before the first frame update
    void Start()
    {
        totalPlayerMoves = 0;
        SetCanvasUI("activeGameplay");
        SetCanvasUI("comboCounterOff");
        startAlpha = 1;
        currAlpha = startAlpha;
    }

    // Update is called once per frame
    void Update()
    {
        if(comboCount >= 5)
        {
            if (ListOfUI[comboIndex].activeSelf == false)
                SetCanvasUI("comboCounterOn");
            //update text
            if (comboTextMesh.text.ToString() != comboCount.ToString())
            {
                comboTextMesh.text = comboCount.ToString();
                
                //configure setting for pulse
                pulse = true;
                pulseTimer = pulseLength;
                comboTextMesh.enableAutoSizing = false;
                defaultTextSize = comboTextMesh.fontSize;                
                pulseToSize = defaultTextSize * 1.5f;
            }
            if (pulse) //make comnbo text swell and shrink
            {
                if (pulseTimer <= 0)
                {
                    comboTextMesh.fontSize = defaultTextSize;
                    comboTextMesh.enableAutoSizing = true;
                    pulse = false;
                }
                else if (pulseTimer > pulseLength / 2) //growing
                {
                    comboTextMesh.fontSize += (pulseToSize - defaultTextSize) * 2 * Time.deltaTime;
                }
                else if (pulseTimer <= pulseLength / 2) //shrinking
                {
                    comboTextMesh.fontSize -= (pulseToSize - defaultTextSize) * 2 * Time.deltaTime;
                }
                pulseTimer -= Time.deltaTime;
            }
        }
        else if (comboCount == 0 && ListOfUI[comboIndex].activeSelf == true)
        {
            SetCanvasUI("comboCounterOff");
        }

        //fade-in fade-out preview panel
        if(canvasState == "previewMode")
        {
            if(fade)
            {
                if (currAlpha <= 0)
                {
                    fade = false;
                    currAlpha = 0;
                }
                else
                {
                    currAlpha -= Time.deltaTime;
                    previewPanel.color = new Color(previewPanel.color.r, previewPanel.color.g, previewPanel.color.b, 0);
                    foreach(TextMeshProUGUI var in previewPanel.GetComponentsInChildren<TextMeshProUGUI>())
                        var.color = new Color(var.color.r, var.color.g, var.color.b, 0);
                    previewControl.color = new Color(1, 1, 1, 0);

                    //swell arrows
                    foreach (Image var in arrows)
                    {
                        Vector3 newScale = var.transform.localScale + (new Vector3(-currAlpha, -currAlpha, 0)/1000);
                        var.transform.localScale = newScale;
                    }
                }
            }
            else
            {
                if(currAlpha >= startAlpha)
                {
                    fade = true;
                    currAlpha = startAlpha;
                }
                else
                {
                    currAlpha += Time.deltaTime;
                    previewPanel.color = new Color(previewPanel.color.r, previewPanel.color.g, previewPanel.color.b, 180);
                    foreach (TextMeshProUGUI var in previewPanel.GetComponentsInChildren<TextMeshProUGUI>())
                        var.color = new Color(var.color.r, var.color.g, var.color.b, 255);
                    previewControl.color = new Color(1, 1, 1, 255);
                    //shrink arrows
                    foreach (Image var in arrows)
                    {
                        Vector3 newScale = var.transform.localScale - (new Vector3(-currAlpha, -currAlpha, 0)/1000);
                        var.transform.localScale = newScale;
                    }
                }
            }
        }
    }

    public void SetCanvasUI(string protocol)
    {
        switch(protocol)
        {
            case "transition":
                {
                    for (int i = 0; i < ListOfUI.Length; i++)
                    {
                        ListOfUI[i].SetActive(false);
                    }
                    break;
                }
            case "previewMode":
                {
                    canvasState = "previewMode";
                    for (int i = 0; i < ListOfUI.Length; i++)
                    {
                        if (ListOfUI[i].name == "previewUI") ListOfUI[i].SetActive(true);
                        else ListOfUI[i].SetActive(false);
                    }
                    break;
                }
            case "activeGameplay":
                {
                    canvasState = "activeGameplay";
                    for(int i = 0; i < ListOfUI.Length; i++)
                    {
                        if (ListOfUI[i].name == "activeGameplayUI") ListOfUI[i].SetActive(true);
                        else ListOfUI[i].SetActive(false);
                    }
                    break;
                }
            case "comboCounterOn":
                {
                    if (comboIndex < 0)
                    {
                        for (int i = 0; i < ListOfUI.Length; i++)
                        {
                            if (ListOfUI[i].name == "comboCounter")
                            {
                                ListOfUI[i].SetActive(true);
                                comboIndex = i;
                            }
                        }
                    }
                    else ListOfUI[comboIndex].SetActive(true);

                    break;
                }
            case "comboCounterOff":
                {
                    if (comboIndex < 0)
                    {
                        for (int i = 0; i < ListOfUI.Length; i++)
                        {
                            if (ListOfUI[i].name == "comboCounter")
                            {
                                ListOfUI[i].SetActive(false);
                                comboIndex = i;
                            }
                        }
                    }
                    else ListOfUI[comboIndex].SetActive(false);

                    break;
                }
            case "dead":
                {
                    canvasState = "dead";
                    for (int i = 0; i < ListOfUI.Length; i++)
                    {
                        if (ListOfUI[i].name == "deathUI") ListOfUI[i].SetActive(true);
                        else ListOfUI[i].SetActive(false);
                    }
                    break;
                }
            case "lvClear":
                {
                    canvasState = "lvClear";
                    for (int i = 0; i < ListOfUI.Length; i++)
                    {
                        if (ListOfUI[i].name == "clearUI")
                        {
                            /*
                             public int levelPar;
                            public TextMeshProUGUI endCombo;
                            public TextMeshProUGUI highestCombo;
                            public TextMeshProUGUI totalMoves;
                            public TextMeshProUGUI levelComboPar;
                            public TextMeshProUGUI gradeText;
                            public int totalPlayerMoves;
                            public int highestPlayerCombo;*/
                            endCombo.text = comboCount.ToString();
                            highestCombo.text = highestPlayerCombo.ToString();
                            totalMoves.text = totalPlayerMoves.ToString();
                            levelComboPar.text = levelPar.ToString();

                            //calculate score
                            float gpa = (float)levelPar / (float)totalPlayerMoves;

                            if (gpa >= 1 && comboCount == totalPlayerMoves)
                            {
                                gradeText.text = "S";
                                //set a color gradient
                                VertexGradient grad = gradeText.colorGradient;
                                grad.bottomLeft = new Color(1, 251/255f,0); //yellow
                                grad.topRight = new Color(1, 251/255f, 0);
                                grad.topLeft = new Color(1,121/255f,0); //orange
                                grad.bottomRight = new Color(1, 121 / 255f, 0);
                                gradeText.colorGradient = grad;
                            }
                            else if (gpa > 0.8)
                            {
                                gradeText.text = "A";
                                gradeText.color = Color.green;
                                VertexGradient grad = gradeText.colorGradient;
                                grad.bottomLeft = new Color(125/255f, 1, 0); //light green
                                grad.topRight = new Color(125/255f, 1, 0);
                                grad.topLeft = new Color(7/255f, 32/255f, 23/255f); //blackish blue
                                grad.bottomRight = new Color(7 / 255f, 32 / 255f, 23 / 255f);
                                gradeText.colorGradient = grad;
                            }
                            else if (gpa > 0.60)
                            {
                                gradeText.text = "B";
                                gradeText.color = Color.blue;
                                VertexGradient grad = gradeText.colorGradient;
                                grad.bottomLeft = new Color(0, 67/255f, 1); //blue
                                grad.topRight = new Color(0, 67 / 255f, 1);
                                grad.topLeft = new Color(70 / 255f, 0, 100 / 255f); //purple
                                grad.bottomRight = new Color(70 / 255f, 0, 100 / 255f);
                                gradeText.colorGradient = grad;
                            }
                            else
                            {
                                gradeText.text = "C";
                                gradeText.color = Color.white;
                                VertexGradient grad = gradeText.colorGradient;
                                grad.bottomLeft = new Color(100/255f, 100/255f, 100/255f); //gray
                                grad.topRight = new Color(100/255f, 100/255f, 100/255f);
                                grad.topLeft = Color.white; //orange
                                grad.bottomRight = Color.white;
                                gradeText.colorGradient = grad;
                            }
                            
                            if(FindObjectOfType<ScoreTracker>())
                            {
                                FindObjectOfType<ScoreTracker>().RecordScore(gradeText.text.ToCharArray()[0]);
                            }

                            ListOfUI[i].SetActive(true);
                        }
                        else ListOfUI[i].SetActive(false);
                    }
                    break;
                }
            default:
                {
                    Debug.Log("CANVAS ERROR: Unidentified SetCanvasUI protocol");
                    break;
                }
        }
    }

    public void SetItemFrame(int itemID)
    {
        //make visible
        itemImage.color = Color.white;
        switch(itemID)
        {
            case 0: //empty slot
                {
                    itemImage.sprite = ItemSprites[itemID];
                    itemImage.color = Color.clear;
                    break;
                }
            case 1: //rock
                {
                    itemImage.sprite = ItemSprites[itemID];
                    break;
                }
            case 2: //water
                {
                    itemImage.sprite = ItemSprites[itemID];
                    break;
                }
            default:
                {
                    Debug.Log("CANVAS ERROR: Unidentified itemID in SetItemFrame");
                    break;
                }
        }

    }

    public void ShowArrow(string arrowDir)
    {
        switch (arrowDir)
        {
            case "right": //0
                {
                    arrowStates[0] = true;
                    arrows[0].color = new Color(1, 1, 1, 1);
                    break;
                }
            case "left": //1
                {
                    arrowStates[1] = true;
                    arrows[1].color = new Color(1, 1, 1, 1);
                    break;
                }
            case "up": //2
                {
                    arrowStates[2] = true;
                    arrows[2].color = new Color(1, 1, 1, 1);
                    break;
                }
            case "down": //3
                {
                    arrowStates[3] = true;
                    arrows[3].color = new Color(1, 1, 1, 1);
                    break;
                }
        }
    }
    public void HideArrow(string arrowDir)
    {
        switch(arrowDir)
        {
            case "right": //0
                {
                    arrowStates[0] = false;
                    arrows[0].color = new Color(1, 1, 1, 0);
                    break;
                }
            case "left": //1
                {
                    arrowStates[1] = false;
                    arrows[1].color = new Color(1, 1, 1, 0);
                    break;
                }
            case "up": //2
                {
                    arrowStates[2] = false;
                    arrows[2].color = new Color(1, 1, 1, 0);
                    break;
                }
            case "down": //3
                {
                    arrowStates[3] = false;
                    arrows[3].color = new Color(1, 1, 1, 0);
                    break;
                }
        }
    }

    public float BeginExitTransition()
    {
        transition.SetTrigger("Start");
        return transitionTime;
    }

}
