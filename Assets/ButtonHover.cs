using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class ButtonHover : MonoBehaviour, IPointerEnterHandler
{
    TitleUI parent;
    public int buttonIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        parent = FindObjectOfType<TitleUI>();

    }

    public void OnPointerEnter(PointerEventData pointerEventData)
    {
        parent.SetActiveButton(buttonIndex);
    }


}
