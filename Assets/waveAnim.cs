using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class waveAnim : MonoBehaviour
{
    SpriteRenderer spr;
    bool fade;
    float fadeTimer = 0;
    public float fadeLen = 3;
    // Start is called before the first frame update
    void Start()
    {
        spr = GetComponent<SpriteRenderer>();
        fade = true;
        fadeTimer = fadeLen;
    }

    // Update is called once per frame
    void Update()
    {
        if(fade == true)
        {
            if(fadeTimer > 0)
            {
                fadeTimer -= Time.deltaTime;
                if (spr.color.a > 0)
                { 
                    float temp = spr.color.a - 0.002f;
                    spr.color = new Color(spr.color.r, spr.color.g, spr.color.b, temp);
                }
            }
            else
            {
                fade = false;
                fadeTimer = fadeLen;
            }
        }
        else
        {
            if (fadeTimer > 0)
            {
                fadeTimer -= Time.deltaTime;
                if (spr.color.a < 1)
                {
                    float temp = spr.color.a + 0.002f;
                    spr.color = new Color(spr.color.r, spr.color.g, spr.color.b, temp);
                }
            }
            else
            {
                fade = true;
                fadeTimer = fadeLen;
            }
        }
    }
}
