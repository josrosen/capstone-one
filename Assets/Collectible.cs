using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collectible : MonoBehaviour
{
    public int itemID;
    public bool collected = false;
    public bool infinite = false;

    void Update()
    {
        if (collected && !infinite)
            Destroy(gameObject);
    }
}
