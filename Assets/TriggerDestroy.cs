using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDestroy : MonoBehaviour
{
    public AudioSource sound;
    bool commence = false;
    bool played = false;

    /*private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
                sound.Play();
                played = true;
        }
    }*/

    private void Start()
    {
        sound.volume = FindObjectOfType<ScoreTracker>().GetMusVolume();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (played)
        {
            if (sound.isPlaying)
            {
                GetComponent<SpriteRenderer>().color = Color.clear;
                if(GetComponent<BoxCollider2D>())
                    GetComponent<BoxCollider2D>().enabled = false;
                if (GetComponent<CircleCollider2D>())
                    GetComponent<CircleCollider2D>().enabled = false;
            }
            else
                Destroy(gameObject);
        }
        else if (commence)
        {
            Destroy(gameObject);
        }
    }

    public void DestroyObject(bool playSound)
    {        
        if (GetComponent<SpawnParticles>())
            GetComponent<SpawnParticles>().callParticles();
        if(playSound && sound != null)
        {
            sound.Play();
            played = true;
        }
        else commence = true;
    }
}
