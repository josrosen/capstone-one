using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimedLogic : MonoBehaviour
{
    public bool canMove = false;
    public bool killAfterMoves;
    public int numMoves;
    int currMoves;
    public float moveSpeed;
    // Start is called before the first frame update
    void Start()
    {
        currMoves = 0;
    }

    public void InvokeMovement(float baseMovSpeed)
    {
        moveSpeed = baseMovSpeed;
        canMove = true;
        currMoves++;

        if (killAfterMoves && currMoves >= numMoves)
            Destroy(this.gameObject);
    }
}
