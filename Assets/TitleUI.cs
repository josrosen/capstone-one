using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class TitleUI : MonoBehaviour
{
    public string canvasState;
    public AudioSource sfx;
    public AudioSource bgm;
    public GameObject[] ListOfUI;
    public Button[] mainButtons;
    public Button[] optionButtons;
    public Button[] creditButtons;
    public Button[] resultButtons;
    public int buttonIndex;

    PlayerMovement pTransform;
    PlayerInput playerInput;
    InGameControls playerInputActions;
    public float inpTimer;
    float enterTimer = 0; //for enter key issues
    float timerLen = 0.2f;
    bool doEvents = true;

    public TextMeshProUGUI startGame;

    //Options
    bool isController = false;
    public TextMeshProUGUI controller;
    public TextMeshProUGUI keyboard;
    public KeyboardPrompt[] keyboardGUI;
    public ControllerPrompt[] controllerGUI;
    int musVolume = 100;
    public TextMeshProUGUI musVol;
    public TextMeshProUGUI musPlus;
    public TextMeshProUGUI musMinus;
    int sndVolume = 100;
    public TextMeshProUGUI sndVol;
    public TextMeshProUGUI sndPlus;
    public TextMeshProUGUI sndMinus;

    //Delay Options
    public TextMeshProUGUI currDelay;
    public TextMeshProUGUI remaningCount;
    public float delayAmt = 0;
    public delayChecker delayPrefab;
    float[] inputDifference = new float[20];
    int delayCheckIndex = 0;
    delayChecker delayObj = null;

    //Credits

    //Rhythm variables
    float latestBeat;
    float BPM = 163;
    float BPS;
    double beatLen;
    double beatTimer = 0;
    public float delay;

    public int currBeatQtr = 0;
    public int currBeatEighth = 0;
    public int totalNotes = 0;
    Vector3 storedScale = Vector3.zero;
    float buttonScalar = 0.3f;
    int newNote = 2;

    //Animation storage
    public FakePlayer playerSequence1;
    public GameObject spawnPoint1;
    public FakePlayer playerSequence2;
    public GameObject spawnPoint2;
    public OneWayHazard barrelSequence1;
    public GameObject spawnPoint3;
    public GameObject spawnPoint4;
    public FakePlayer playerSequence3;
    public OneWayHazard barrelSequence2;

    //Scene Manager
    //Transition animation
    public float transitionTime = 0.5f;
    public Animator transition;
    Scene currScene;
    Scene nextScene;
    private IEnumerator sceneLoader;

    // Start is called before the first frame update
    void Start()
    {
        

        playerInput = GetComponent<PlayerInput>();
        playerInputActions = new InGameControls();
        playerInputActions.Menu.Enable();

        if (resultButtons[0] == null)
        {
            canvasState = "main";
            for (int i = 0; i < ListOfUI.Length; i++)
            {
                if (ListOfUI[i].name == "MainMenu")
                    ListOfUI[i].SetActive(true);
                else ListOfUI[i].SetActive(false);
            }
            buttonIndex = 0;
            inpTimer = 0;
        }
        else
        {
            doEvents = false;
            canvasState = "results";
            for (int i = 0; i < ListOfUI.Length; i++)
            {
                if(ListOfUI[i].name == "ResultsMenu")
                    ListOfUI[i].SetActive(true);
                else ListOfUI[i].SetActive(false);
            }
            

        }


        //set values from ScoreTracker
        ScoreTracker tracker = FindObjectOfType<ScoreTracker>();
        musVolume = (int)(tracker.GetMusVolume() * 100);
        sndVolume = (int)(tracker.GetSfxVolume() * 100);
        isController = tracker.GetInputType();
        delayAmt = tracker.GetDelay();

        if (doEvents && tracker.GetCurrentRoom() != "1 BasicMovement")
            startGame.text = "Continue Game";

        keyboardGUI = FindObjectsOfType<KeyboardPrompt>();
        controllerGUI = FindObjectsOfType<ControllerPrompt>();
        if(!isController)
            foreach (ControllerPrompt key in controllerGUI)
                key.gameObject.SetActive(false);
        else
            foreach (KeyboardPrompt key in keyboardGUI)
                key.gameObject.SetActive(false);
        //text management
        bgm.volume = musVolume / 100f;
        musVol.text = musVolume.ToString();
        if (musVolume == 100)
            musPlus.color = new Color(0, 0, 0, .5f);
        else if (musVolume == 0)
            musMinus.color = new Color(0, 0, 0, .5f);
        //text management
        sfx.volume = sndVolume / 100f;
        sndVol.text = sndVolume.ToString();
        if (sndVolume == 100)
            sndPlus.color = new Color(0, 0, 0, .5f);
        else if (sndVolume == 0)
            sndMinus.color = new Color(0, 0, 0, .5f);

        //rhythm
        BPS = BPM / 60f;
        beatLen = 60f / BPM;

        //make animation swipe transparent 
        //transition.gameObject.GetComponent<Image>().color = new Color(0, 0, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        //keep duplicate inputs from being caught
        if(inpTimer > 0)
            inpTimer -= Time.deltaTime;
        if (enterTimer > 0)
            enterTimer -= Time.deltaTime;

        //Conduct based on the bgm
        if (beatTimer < beatLen)
        {
            beatTimer += Time.deltaTime;
            //shrink active button in time with the music
            if (currBeatEighth != GetBeatRemainingEighth())
            {
                currBeatEighth = GetBeatRemainingEighth();
                newNote = 1; //start increasing
            }
            else if(newNote == 2) 
                newNote = 0; //start decreasing
        }
        else
        {
            totalNotes++;
            if (totalNotes > 87)
                totalNotes = 0;
            if (!bgm.isPlaying)
            {
                bgm.Play();
                totalNotes = 0;
            }
            beatTimer = (beatTimer - beatLen) + Time.deltaTime; //accounting for the extra difference from deltaTime
            latestBeat = Time.realtimeSinceStartup;
            currBeatQtr = currBeatQtr < 3 ? currBeatQtr + 1 : 0;
            //currBeatEighth = 0;
            //currBeatSixteenth = 0;
            newNote = 2;
            AcceptMovement();
        }

        //spawn events depending on beat
        if (doEvents)
        {
            if (totalNotes == 7 && !FindObjectOfType<FakePlayer>())
            {
                Instantiate(playerSequence1, spawnPoint1.transform);
            }
            if (totalNotes == 30 && !FindObjectOfType<FakePlayer>())
            {
                Instantiate(playerSequence2, spawnPoint1.transform);
                Instantiate(barrelSequence1, spawnPoint2.transform);
            }
            if (totalNotes == 58 && !FindObjectOfType<FakePlayer>())
            {
                Instantiate(playerSequence3, spawnPoint3.transform);
            }
            if (totalNotes == 69 && !FindObjectOfType<OneWayHazard>())
            {
                Instantiate(barrelSequence2, spawnPoint4.transform);
            }
        }

        //focus on specific menu
        switch (canvasState)
        {
            case "main":
                {
                    //scale button with beat
                    if(newNote == 0) //increase
                    {
                        mainButtons[buttonIndex].transform.localScale += new Vector3(buttonScalar*Time.deltaTime, buttonScalar*Time.deltaTime, 0);
                    }
                    else if (newNote == 1) //decrease
                    {
                        mainButtons[buttonIndex].transform.localScale -= new Vector3(buttonScalar * Time.deltaTime, buttonScalar * Time.deltaTime, 0);
                    }
                    else //2 means reset to base scale
                    {
                        mainButtons[buttonIndex].transform.localScale = new Vector3(1, 1, 1);
                    }
                    
                    //Accept press from keyboard/controller
                    if (inpTimer <= 0)
                    {
                        if (playerInputActions.Menu.Confirm.WasPerformedThisFrame())
                        {
                            inpTimer = timerLen;
                            mainButtons[buttonIndex].onClick.Invoke();
                        }
                        //if back is pushed, focus on the exit game button
                        if (playerInputActions.Menu.Back.WasPerformedThisFrame())
                        {
                            inpTimer = timerLen;
                            Vector3 currScale = mainButtons[buttonIndex].transform.localScale;
                            mainButtons[buttonIndex].transform.localScale = new Vector3(1, 1, 1);
                            buttonIndex = mainButtons.Length - 1;
                            mainButtons[buttonIndex].transform.localScale = currScale;
                            sfx.Play();
                        }
                        //catch directional input from keyboard/controller
                        if (playerInputActions.Menu.Movement.ReadValue<Vector2>().y != 0)
                        {
                            inpTimer = timerLen;
                            Vector3 currScale = mainButtons[buttonIndex].transform.localScale;
                            mainButtons[buttonIndex].transform.localScale = new Vector3(1, 1, 1);
                            buttonIndex -= (int)(Mathf.Ceil(playerInputActions.Menu.Movement.ReadValue<Vector2>().y));
                            
                            if (buttonIndex > mainButtons.Length - 1)
                                buttonIndex = 0;
                            if (buttonIndex < 0)
                                buttonIndex = mainButtons.Length - 1;
                            mainButtons[buttonIndex].transform.localScale = currScale;
                        }

                    }

                    mainButtons[buttonIndex].Select();
                    break;
                }
            case "options":
                {
                    //scale button with beat
                    if (newNote == 0) //increase
                    {
                        optionButtons[buttonIndex].transform.localScale += new Vector3(buttonScalar * Time.deltaTime, buttonScalar * Time.deltaTime, 0);
                    }
                    else if (newNote == 1) //decrease
                    {
                        optionButtons[buttonIndex].transform.localScale -= new Vector3(buttonScalar * Time.deltaTime, buttonScalar * Time.deltaTime, 0);
                    }
                    else //2 means reset to base scale
                    {
                        optionButtons[buttonIndex].transform.localScale = new Vector3(1, 1, 1);
                    }

                    //Accept press from keyboard/controller
                    if (inpTimer <= 0)
                    {
                        if (playerInputActions.Menu.Confirm.WasPerformedThisFrame())
                        {
                            optionButtons[buttonIndex].onClick.Invoke();
                            inpTimer = timerLen;
                        }
                        if(playerInputActions.Menu.Back.WasPerformedThisFrame())
                        {
                            //go back to main menu
                            inpTimer = timerLen;
                            SetCanvasUI("main");
                            sfx.Play();
                        }
                        //catch directional input from keyboard/controller
                        if (playerInputActions.Menu.Movement.ReadValue<Vector2>().y != 0)
                        {
                            inpTimer = timerLen;
                            Vector3 currScale = optionButtons[buttonIndex].transform.localScale;
                            optionButtons[buttonIndex].transform.localScale = new Vector3(1, 1, 1);
                            buttonIndex -= (int)(Mathf.Ceil(playerInputActions.Menu.Movement.ReadValue<Vector2>().y));
                            if (buttonIndex > optionButtons.Length - 1)
                                buttonIndex = 0;
                            if (buttonIndex < 0)
                                buttonIndex = optionButtons.Length - 1;
                            optionButtons[buttonIndex].transform.localScale = currScale;
                        }
                    }
                    optionButtons[buttonIndex].Select();
                    break;
                }
            case "musVol":
                {
                    //scale button with beat (even though it's not on-screen)
                    if (newNote == 0) //increase
                    {
                       storedScale += new Vector3(buttonScalar * Time.deltaTime, buttonScalar * Time.deltaTime, 0);
                    }
                    else if (newNote == 1) //decrease
                    {
                        storedScale -= new Vector3(buttonScalar * Time.deltaTime, buttonScalar * Time.deltaTime, 0);
                    }
                    else //2 means reset to base scale
                    {
                        storedScale = new Vector3(1, 1, 1);
                    }

                    //text management
                    if (musVolume == 100)
                        musPlus.color = new Color(0, 0, 0, .5f);
                    else if (musVolume == 0)
                        musMinus.color = new Color(0, 0, 0, .5f);
                    else
                    {
                        musPlus.color = new Color(.9921f, .5725f, 0);
                        musMinus.color = new Color(.9921f, .5725f, 0);
                    }

                    if (inpTimer <= 0)
                    {
                        //enter key works innately with buttons but not in this instance, back button also terminates the same
                        if (playerInputActions.Menu.Confirm.WasPerformedThisFrame() || (enterTimer <= 0 && playerInputActions.Menu.Enter.WasPressedThisFrame()) || playerInputActions.Menu.Back.WasPerformedThisFrame())
                        {
                            

                            //revert text
                            if (musVolume == 100)
                                musPlus.color = new Color(0, 0, 0, 0.5f);
                            else musPlus.color = new Color(1, 1, 1, 1);
                            if (musVolume == 0)
                                musMinus.color = new Color(0, 0, 0, 0.5f);
                            else musMinus.color = new Color(1, 1, 1, 1);
                            musVol.color = new Color(1, 1, 1, 1);

                            //return to options menu
                            foreach (Button butt in optionButtons)
                            {
                                butt.enabled = true;
                            }
                            canvasState = "options";
                            buttonIndex = 1;
                            inpTimer = timerLen;
                            sfx.Play();
                        }
                        //catch directional input from keyboard/controller
                        if (playerInputActions.Menu.Movement.ReadValue<Vector2>().x != 0)
                        {
                            inpTimer = timerLen/3;
                            musVolume += (int)(Mathf.Ceil(playerInputActions.Menu.Movement.ReadValue<Vector2>().x));
                            if (musVolume > 100)
                                musVolume = 100;
                            if (musVolume < 0)
                                musVolume = 0;

                            //update volume levels to audio sources
                            bgm.volume = musVolume / 100f;
                            musVol.text = musVolume.ToString();
                        }
                    }
                    break;
                }
            case "sndVol":
                {
                    //scale button with beat (even though it's not on-screen)
                    if (newNote == 0) //increase
                    {
                        storedScale += new Vector3(buttonScalar * Time.deltaTime, buttonScalar * Time.deltaTime, 0);
                    }
                    else if (newNote == 1) //decrease
                    {
                        storedScale -= new Vector3(buttonScalar * Time.deltaTime, buttonScalar * Time.deltaTime, 0);
                    }
                    else //2 means reset to base scale
                    {
                        storedScale = new Vector3(1, 1, 1);
                    }

                    //text management
                    if (sndVolume == 100)
                        sndPlus.color = new Color(0, 0, 0, .5f);
                    else if (sndVolume == 0)
                        sndMinus.color = new Color(0, 0, 0, .5f);
                    else
                    {
                        sndPlus.color = new Color(.9921f, .5725f, 0);
                        sndMinus.color = new Color(.9921f, .5725f, 0);
                    }

                    if (inpTimer <= 0)
                    {
                        //enter key works innately with buttons but not in this instance, back button also terminates the same
                        if (playerInputActions.Menu.Confirm.WasPerformedThisFrame() || (enterTimer <= 0 && playerInputActions.Menu.Enter.WasPressedThisFrame()) || playerInputActions.Menu.Back.WasPerformedThisFrame())
                        {
                            //revert text
                            if (sndVolume == 100)
                                sndPlus.color = new Color(0, 0, 0, 0.5f);
                            else sndPlus.color = new Color(1, 1, 1, 1);
                            if (sndVolume == 0)
                                sndMinus.color = new Color(0, 0, 0, 0.5f);
                            else sndMinus.color = new Color(1, 1, 1, 1);
                            sndVol.color = new Color(1, 1, 1, 1);

                            //return to options menu
                            foreach (Button butt in optionButtons)
                            {
                                butt.enabled = true;
                            }
                            canvasState = "options";
                            buttonIndex = 2;
                            inpTimer = timerLen;
                            sfx.Play();
                        }
                        //catch directional input from keyboard/controller
                        if (playerInputActions.Menu.Movement.ReadValue<Vector2>().x != 0)
                        {
                            inpTimer = timerLen / 3;
                            sndVolume += (int)(Mathf.Ceil(playerInputActions.Menu.Movement.ReadValue<Vector2>().x));
                            if (sndVolume > 100)
                                sndVolume = 100;
                            if (sndVolume < 0)
                                sndVolume = 0;

                            //update volume levels to audio sources
                            sfx.volume = sndVolume / 100f;
                            if (!sfx.isPlaying)
                                sfx.Play();
                            sndVol.text = sndVolume.ToString();
                        }
                    }
                    break;
                }
            case "delay":
                {
                    //scale button with beat (even though it's not on-screen)
                    if (newNote == 0) //increase
                    {
                        storedScale += new Vector3(buttonScalar * Time.deltaTime, buttonScalar * Time.deltaTime, 0);
                    }
                    else if (newNote == 1) //decrease
                    {
                        storedScale -= new Vector3(buttonScalar * Time.deltaTime, buttonScalar * Time.deltaTime, 0);
                    }
                    else //2 means reset to base scale
                    {
                        storedScale = new Vector3(1, 1, 1);
                    }

                    if (delayCheckIndex < 20)
                    {
                        //take an input and add to the array
                        if (playerInputActions.Menu.Confirm.WasPressedThisFrame() || (playerInputActions.Menu.Enter.WasPressedThisFrame() && enterTimer <= 0))
                        {
                            inputDifference[delayCheckIndex] = delayObj.AcceptInput();
                            delayCheckIndex++;
                            remaningCount.text = (20 - delayCheckIndex).ToString();
                        }
                        if (playerInputActions.Menu.Back.WasPressedThisFrame())
                        {
                            //terminate and return to default
                            delayAmt = 0;
                            SetCanvasUI("options");
                            Destroy(delayObj.gameObject);
                            delayObj = null;
                            sfx.Play();
                            bgm.mute = false;
                            
                        }

                    }
                    else
                    {
                        //terminate
                        if (delayObj)
                        {
                            float delayAvg = 0;
                            for(int i = 0; i < 20; i++)
                            {
                                delayAvg += inputDifference[i];
                            }
                            delayAvg /= 20;
                            delayAvg -= 100; //from my testing, there is about a 100ms disparity
                            if (delayAvg > 50)
                                delayAvg = 50;
                            if (delayAvg < -50)
                                delayAvg = -50;

                            remaningCount.text = ((int)(delayAvg)).ToString() + "ms";
                            delayAmt = delayAvg/1000;

                            Destroy(delayObj.gameObject);
                            delayObj = null;
                        }

                        //confirm or back close out of the menu
                        if(playerInputActions.Menu.Back.WasPerformedThisFrame() || playerInputActions.Menu.Confirm.WasPressedThisFrame() || playerInputActions.Menu.Enter.WasPressedThisFrame())
                        {
                            SetCanvasUI("options");
                            enterTimer = 0.1f;
                            sfx.PlayDelayed(delayAmt);
                            bgm.mute = false;
                        }
                    }
                    break;
                }
            case "credits":
                {
                    //scale button with beat
                    if (newNote == 0) //increase
                    {
                        creditButtons[buttonIndex].transform.localScale += new Vector3(buttonScalar * Time.deltaTime, buttonScalar * Time.deltaTime, 0);
                    }
                    else if (newNote == 1) //decrease
                    {
                        creditButtons[buttonIndex].transform.localScale -= new Vector3(buttonScalar * Time.deltaTime, buttonScalar * Time.deltaTime, 0);
                    }
                    else //2 means reset to base scale
                    {
                        creditButtons[buttonIndex].transform.localScale = new Vector3(1, 1, 1);
                    }

                    //Accept press from keyboard/controller
                    if (inpTimer <= 0 && (playerInputActions.Menu.Confirm.WasPerformedThisFrame() || playerInputActions.Menu.Back.WasPerformedThisFrame()))
                    {
                        creditButtons[buttonIndex].onClick.Invoke();
                        inpTimer = timerLen;
                    }
                    creditButtons[buttonIndex].Select();
                    break;
                }
            case "start":
                {

                    break;
                }
            case "quit":
                {
                    if (!sfx.isPlaying)
                        Application.Quit();
                    break;
                }
            case "results":
                {
                    //scale button with beat
                    if (newNote == 0) //increase
                    {
                        resultButtons[buttonIndex].transform.localScale += new Vector3(buttonScalar * Time.deltaTime, buttonScalar * Time.deltaTime, 0);
                    }
                    else if (newNote == 1) //decrease
                    {
                        resultButtons[buttonIndex].transform.localScale -= new Vector3(buttonScalar * Time.deltaTime, buttonScalar * Time.deltaTime, 0);
                    }
                    else //2 means reset to base scale
                    {
                        resultButtons[buttonIndex].transform.localScale = new Vector3(1, 1, 1);
                    }

                    //Accept press from keyboard/controller
                    if (inpTimer <= 0 && (playerInputActions.Menu.Confirm.WasPerformedThisFrame() || playerInputActions.Menu.Back.WasPerformedThisFrame()))
                    {
                        resultButtons[buttonIndex].onClick.Invoke();
                        inpTimer = timerLen;
                    }
                    resultButtons[buttonIndex].Select();
                    break;
                }
        }
    }

    public void SetCanvasUI(string newState)
    {
        enterTimer = 0.5f;
        switch(newState)
        {
            case "main":
                {
                    //check which menu we're returning from to pull stored scale
                    if(canvasState == "options")
                    {
                        storedScale = optionButtons[buttonIndex].transform.localScale;
                        optionButtons[buttonIndex].transform.localScale = new Vector3(1, 1, 1);
                    }
                    else if(canvasState == "credits")
                    {
                        storedScale = creditButtons[buttonIndex].transform.localScale;
                        creditButtons[buttonIndex].transform.localScale = new Vector3(1, 1, 1);
                    }
                    canvasState = newState;
                    for (int i = 0; i < ListOfUI.Length; i++)
                    {
                        if (ListOfUI[i].name == "MainMenu") ListOfUI[i].SetActive(true);
                        else ListOfUI[i].SetActive(false);
                    }
                    buttonIndex = 0;
                    mainButtons[buttonIndex].transform.localScale = storedScale;
                    break;
                }
            case "start":
                {
                    canvasState = newState;
                    break;
                }
            case "options":
                {
                    //check which menu we're returning from to pull stored scale
                    if (canvasState == "main")
                    {
                        storedScale = mainButtons[buttonIndex].transform.localScale;
                        mainButtons[buttonIndex].transform.localScale = new Vector3(1, 1, 1);
                    }
                    buttonIndex = 0;
                    canvasState = newState;
                    for (int i = 0; i < ListOfUI.Length; i++)
                    {
                        if (ListOfUI[i].name == "OptionsMenu") ListOfUI[i].SetActive(true);
                        else ListOfUI[i].SetActive(false);
                    }

                    optionButtons[buttonIndex].transform.localScale = storedScale;

                    //display variables need to be retrieved
                    if(isController)
                    {
                        controller.gameObject.SetActive(true);
                        keyboard.gameObject.SetActive(false);
                    }
                    else
                    {
                        keyboard.gameObject.SetActive(true);
                        controller.gameObject.SetActive(false);
                    }
                    currDelay.text = ((int)(delayAmt*1000)).ToString() + "ms";
                    break;
                }
            case "musVol":
                {
                    storedScale = optionButtons[buttonIndex].transform.localScale;
                    optionButtons[buttonIndex].transform.localScale = new Vector3(1, 1, 1);

                    canvasState = newState;
                    foreach(Button butt in optionButtons)
                    {
                        butt.enabled = false;
                    }
                    musVol.color = new Color(.9921f, .5725f, 0); //orange of other text
                    musPlus.color = new Color(.9921f, .5725f, 0);
                    musMinus.color = new Color(.9921f, .5725f, 0);
                    break;
                }
            case "sndVol":
                {
                    storedScale = optionButtons[buttonIndex].transform.localScale;
                    optionButtons[buttonIndex].transform.localScale = new Vector3(1, 1, 1);

                    canvasState = newState;
                    foreach (Button butt in optionButtons)
                    {
                        butt.enabled = false;
                    }
                    sndVol.color = new Color(.9921f, .5725f, 0); //orange of other text
                    sndPlus.color = new Color(.9921f, .5725f, 0);
                    sndMinus.color = new Color(.9921f, .5725f, 0);
                    break;
                }
            case "delay":
                {
                    storedScale = optionButtons[buttonIndex].transform.localScale;
                    optionButtons[buttonIndex].transform.localScale = new Vector3(1, 1, 1);

                    canvasState = newState;
                    for (int i = 0; i < ListOfUI.Length; i++)
                    {
                        if (ListOfUI[i].name == "DelayMenu") ListOfUI[i].SetActive(true);
                        else ListOfUI[i].SetActive(false);
                    }
                    bgm.mute = true;

                    delayObj = Instantiate(delayPrefab);
                    delayCheckIndex = 0;

                    enterTimer = 0.1f;
                    remaningCount.text = (20).ToString();
                    break;
                }
            case "credits":
                {
                    storedScale = mainButtons[buttonIndex].transform.localScale;
                    mainButtons[buttonIndex].transform.localScale = new Vector3(1, 1, 1);

                    buttonIndex = 0;
                    canvasState = newState;
                    for (int i = 0; i < ListOfUI.Length; i++)
                    {
                        if (ListOfUI[i].name == "CreditsMenu") ListOfUI[i].SetActive(true);
                        else ListOfUI[i].SetActive(false);
                    }

                    creditButtons[buttonIndex].transform.localScale = storedScale;
                    break;
                }
            case "quit":
                {
                    canvasState = newState;
                    break;
                }
        }
    }

    public void StartGame()
    {
        //SetCanvasUI("start");

        //transfer all settings to doNotDestroyOnLoad object
        FindObjectOfType<ScoreTracker>().SetConfigurations(isController, musVolume, sndVolume, delayAmt);
        LoadRoom(FindObjectOfType<ScoreTracker>().GetCurrentRoom()); 
    }

    public void QuitGame(AudioSource buttonSound)
    {
        SetCanvasUI("quit");
        sfx.Play();
    }

    public void ReturnToMain()
    {
        bgm.mute = true;
        LoadRoom("0 Title");
    }

    public void SetActiveButton(int index)
    {
        buttonIndex = index;
    }

    //Options functions
    public void SwitchControlType()
    {
        if (canvasState == "options")
        {
            if (isController)
            {
                isController = false;
                controller.gameObject.SetActive(false);
                keyboard.gameObject.SetActive(true);
                //do the same for everything else
                foreach (KeyboardPrompt key in keyboardGUI)
                    key.gameObject.SetActive(true);
                foreach (ControllerPrompt key in controllerGUI)
                    key.gameObject.SetActive(false);
            }
            else
            {
                isController = true;
                keyboard.gameObject.SetActive(false);
                controller.gameObject.SetActive(true);
                //do the same for everything else
                foreach (KeyboardPrompt key in keyboardGUI)
                    key.gameObject.SetActive(false);
                foreach (ControllerPrompt key in controllerGUI)
                    key.gameObject.SetActive(true);
            }
        }
    }

    //conductor functions
    public void AcceptMovement()
    {
        TimedLogic[] movingObjs = GameObject.FindObjectsOfType<TimedLogic>();
        for (int i = 0; i < movingObjs.Length; i++)
        {
            movingObjs[i].InvokeMovement(GetMoveSpeed());
        }
        PulseOnBeat[] pulsingObjs = GameObject.FindObjectsOfType<PulseOnBeat>();
        for (int i = 0; i < pulsingObjs.Length; i++)
        {
            pulsingObjs[i].SetPulse();
        }
    }
    public float GetMoveSpeed()
    {
        return (float)((2 * BPS) / beatLen);
    }
    public int GetBeatRemainingEighth()
    {
        double timeMath = beatTimer + delay;
        currBeatEighth = (timeMath > beatLen / 2) ? 1 : 0;
        currBeatEighth += currBeatQtr * 2;
        //Debug.Log("Eighth = " + currBeatEighth + "/8");
        return currBeatEighth;
    }

    //Scene Management
    public float BeginExitTransition()
    {
        transition.gameObject.GetComponent<Image>().color = new Color(0, 0, 0, 1);
        transition.SetTrigger("Start");
        return transitionTime;
    }
    //Room Loading

    public void LoadRoom(string roomName)
    {

        /*if (FindObjectOfType<PlayerMovement>() != null)
            playerPos = FindObjectOfType<PlayerScript>().transform.position;*/
        //FindObjectOfType<EventSystem>().enabled = false;
        if (roomName != "0 Title")
        {
            FindObjectOfType<ScoreTracker>().SetCurrentRoom(roomName);
        }
        else if (doEvents)
            FindObjectOfType<ScoreTracker>().SetCurrentRoom("1 BasicMovement");
        sceneLoader = LoadingRoom(roomName);
        StartCoroutine(sceneLoader);
    }
    private IEnumerator LoadingRoom(string roomName)
    {
        //Send transition request to canvas then wait for finish
        yield return new WaitForSeconds(BeginExitTransition());


        //DontDestroyOnLoad(gameObject);
        //DontDestroyOnLoad(overworldUI);
        SceneManager.LoadScene(roomName, LoadSceneMode.Single);
        AsyncOperation nScene = SceneManager.LoadSceneAsync(roomName, LoadSceneMode.Single);
        nScene.allowSceneActivation = false;
        while (nScene.progress < 0.9f)
        {
            yield return null;
        }

        nScene.allowSceneActivation = true;

        while (!nScene.isDone)
        {
            yield return null;
        }

        

        nextScene = SceneManager.GetSceneByName(roomName);
        if (nextScene.IsValid())
        {
            //SceneManager.MoveGameObjectToScene(gameObject, nextScene);
            SceneManager.SetActiveScene(nextScene);
        }
    }
}
