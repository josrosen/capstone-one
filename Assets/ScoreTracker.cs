using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScoreTracker : MonoBehaviour
{
    public int numLevels = 1;
    public char[] ScoreArray;

    string currLevel = "1 BasicMovement";
    bool controller = false;
    float musicVol = 100;
    float sndVol = 100;
    float delay = 0;

    Scene currScene;
    
    // Start is called before the first frame update
    void Start()
    {
        if (FindObjectsOfType<ScoreTracker>().Length > 1)
            Destroy(gameObject);
        ScoreArray = new char[numLevels];
        currScene = SceneManager.GetActiveScene();
        DontDestroyOnLoad(gameObject);
    }

    public void RecordScore(char result)
    {
        currScene = SceneManager.GetActiveScene();
        int level = currScene.buildIndex;
        ScoreArray[level - 1] = result;
    }

    public void SetConfigurations(bool isController, float musVol, float sfxVol, float delayAmt)
    {
        controller = isController;
        musicVol = musVol;
        sndVol = sfxVol;
        delay = delayAmt;
    }

    public string GetCurrentRoom()
    {
        return currLevel;
    }
    public void SetCurrentRoom(string roomName)
    {
        currLevel = roomName;
    }

    public bool GetInputType()
    {
        return controller;
    }
    public float GetMusVolume()
    {
        return musicVol/100f;
    }
    public float GetSfxVolume()
    {
        return sndVol/100f;
    }

    public float GetDelay()
    {
        return delay;
    }
}
