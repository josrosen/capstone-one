using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    //Control variables (subject to change)
    public bool canMove = false; //Lets player put in inputs
    int beatPerformed = -1;
    public float bufferLen = 0.2f;          //Gives time for player to enter additional inputs before disabled
    public float buffTime;
    string mainInput;
    public string playState;
    string deathType;
    float deathTimer = 0;
    float checkDeath = 0;

    //input triggers
    PlayerInput playerInput;
    InGameControls playerInputActions;
    bool isGrounded = true;
    public bool dashTriggered = false;
    bool itemState = false;
    int facing = 1;
    int left = 0;
    int right = 0;
    int up = 0;
    int down = 0;
    bool playSound = true;



    public bool hasKey;
    bool goalHit = false;
    Coroutine movement;
    Coroutine moveQueue;
    Coroutine playerAnim;
    public float travelDistance;
    public float moveSpeed; //in seconds
    public int fallingLen;

    public int stickyTaps = 0;
    bool collided = false;

    public GameObject[] itemList;
    GameObject spawnedItem;
    int currItemID;
    public bool itemHeld = false;

    SpriteRenderer spr;
    public Sprite sprLeft;
    public Sprite sprRight;
    public Sprite sprUp;
    public Sprite sprDown;

    //Sound Collection
    float soundDelay;
    public AudioSource soundTone;
    public AudioSource secondSource;
    private AudioClip empty = null;
    public AudioClip moveSound;
    public AudioClip fallSound;
    public AudioClip wallBump;
    public AudioClip dashSound;
    public AudioClip wallSmash;
    public AudioClip itemPickup;
    public AudioClip barrelDeath;
    public AudioClip fireWoosh;
    //public AudioClip keyGrab;
    //public AudioClip doorOpen;


    //Objects to reference
    GameController gameCon;
    private Vector3 previousSpot;
    private Vector3 nextSpot;
    private int travelDirection = 0;
    SoundPlayer sound;
    public ParticleSystem hitSystem;

    // Start is called before the first frame update
    void Start()
    {
        previousSpot = transform.position;
        gameCon = FindObjectOfType<GameController>();
        sound = FindObjectOfType<SoundPlayer>();
        spr = GetComponent<SpriteRenderer>();
        mainInput = "";
        playerInput = GetComponent<PlayerInput>();
        playerInputActions = new InGameControls();
        playerInputActions.ArenaMovement.Disable();

        playState = "preview";
        moveSpeed = ((float)gameCon.BPM / 60) / 0.5f;

        soundDelay = FindObjectOfType<ScoreTracker>().GetDelay();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (playState != "dead")
        {
            switch (other.tag)
            {
                case "CollectibleItem":
                    {
                        if (!itemHeld)
                        {
                            soundTone.clip = itemPickup;
                            soundTone.PlayDelayed(soundDelay);
                            //sound.InvokePlayerSound(itemPickup);
                            itemHeld = true;
                            currItemID = other.GetComponent<Collectible>().itemID;
                            other.GetComponent<Collectible>().collected = true;
                            gameCon.InsertItemUI(currItemID);
                        }
                        break;
                    }
                case "StickyTrap":
                    {
                        playState = "stuck";
                        stickyTaps = 2;
                        //cancel a dash if the player would try to cleaer through
                        if (dashTriggered && (Mathf.Abs(transform.position.x - nextSpot.x) > 1 || Mathf.Abs(transform.position.y - nextSpot.y) > 1)) //don't make player move back two tiles if dashing
                        {
                            switch (facing)
                            {
                                case 0: //up
                                    {
                                        nextSpot.y -= 1;
                                        break;
                                    }
                                case 1: //down
                                    {
                                        nextSpot.y += 1;
                                        break;
                                    }
                                case 2: //left
                                    {
                                        nextSpot.x += 1;
                                        break;
                                    }
                                case 3: //right
                                    {
                                        nextSpot.x -= 1;
                                        break;
                                    }
                            }
                            dashTriggered = false;
                        }
                        left = 0; right = 0; up = 0; down = 0;
                        other.GetComponent<TriggerDestroy>().DestroyObject(true);
                        break;
                    }
                case "SnapTrap":
                    {
                        if (!dashTriggered || (Mathf.Abs(transform.position.x - nextSpot.x) < 1 && Mathf.Abs(transform.position.y - nextSpot.y) < 1))
                        {
                            sound.InvokePrioritySound(null);
                            playState = "dead";
                            deathType = "snapped";
                        }
                        break;
                    }
                case "Barrel":
                    {
                        sound.InvokePrioritySound(null);
                        playState = "dead";
                        if (other.GetComponent<OneWayHazard>().axis == 'x')
                            deathType = "rolledX";
                        else
                            deathType = "rolledY";
                        break;
                    }
                case "Pitfall":
                    {
                        if (!dashTriggered || (Mathf.Abs(transform.position.x - nextSpot.x) < 1 && Mathf.Abs(transform.position.y - nextSpot.y) < 1))
                        {
                            sound.InvokePrioritySound(fallSound);
                            playState = "dead";
                            deathType = "pitfall";
                        }
                        break;
                    }
                case "Fire":
                    {
                        sound.InvokePrioritySound(fireWoosh);
                        playState = "dead";
                        deathType = "fire";
                        break;
                    }
                case "BasicCollision":
                    {
                        if(dashTriggered) //destroy wall
                        {
                            //sound.InvokePlayerSound(wallSmash);
                            soundTone.clip = wallSmash;
                            soundTone.PlayDelayed(soundDelay);
                            other.GetComponent<TriggerDestroy>().DestroyObject(true);
                        }
                        else //treat as basic collision
                        {
                            collided = true;
                            if (nextSpot != previousSpot)
                            {
                                //sound.InvokePlayerSound(wallBump);
                                soundTone.clip = wallBump;
                                soundTone.PlayDelayed(soundDelay);
                                gameCon.ResetCombo();
                                //check if player was knocked out of alignmnent
                                if (previousSpot.x % 1 != 0 || previousSpot.y % 1 != 0)
                                {
                                    Debug.Log("WARNING! Player prev position is not divisible by 1");
                                }
                                nextSpot = previousSpot;
                                previousSpot = transform.position;
                            }
                        }
                        break;
                    }
                case "Finish":
                    {
                        playState = "victoryAnim";
                        other.GetComponent<GoalTrigger>().beginConfetti();
                        //playState = "lvClear";
                        if (GetComponentInChildren<beatIndicator>())
                            Destroy(GetComponentInChildren<beatIndicator>().gameObject);
                        if (GetComponentInChildren<StickyTrapUI>())
                            Destroy(GetComponentInChildren<StickyTrapUI>().gameObject);
                        break;
                    }
                default: Debug.Log("Unidentified Trigger Collision Detected!"); break;
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (playState != "dead")
        {
            switch (collision.tag) 
            {
                case "CollectibleItem":
                    {
                        if (!itemHeld && mainInput == "")
                        {
                            itemHeld = true;
                            currItemID = collision.GetComponent<Collectible>().itemID;
                            collision.GetComponent<Collectible>().collected = true;
                            gameCon.InsertItemUI(currItemID);
                        }
                        break;
                    }
                case "Pitfall":
                    {
                        if (collided && (Mathf.Abs(transform.position.x - nextSpot.x) < 1 && Mathf.Abs(transform.position.y - nextSpot.y) < 1))
                        {
                            sound.InvokePrioritySound(fallSound);
                            playState = "dead";
                            deathType = "pitfall";
                        }
                        break;
                    }
            } 
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.tag == "BasicCollision" && !collided)
        {
            checkDeath = 0;
            collided = true;
            if (nextSpot != previousSpot)
            {
                bool destroyedWall;
                if (soundTone.clip == wallSmash && soundTone.isPlaying)
                    destroyedWall = true;
                else
                {
                    destroyedWall = false;
                    soundTone.clip = wallBump;
                    soundTone.PlayDelayed(soundDelay);
                }
                //check if player was knocked out of alignmnent
                if(previousSpot.x % 1 != 0 || previousSpot.y % 1 != 0)
                {
                    Debug.Log("WARNING! Player prev position is not divisible by 1");
                    
                }
                if(dashTriggered && (Mathf.Abs(previousSpot.x - transform.position.x) > 1 || Mathf.Abs(previousSpot.y - transform.position.y) > 1)) //don't make player move back two tiles if dashing
                {
                    if (!collision.collider.GetComponent<ignoreCombo>() && !destroyedWall)
                    {
                        dashTriggered = false;
                        gameCon.ResetCombo();
                    }
                    switch (facing)
                    {
                        case 0: //up
                            {
                                previousSpot.y += 1;
                                break;
                            }
                        case 1: //down
                            {
                                previousSpot.y -= 1;
                                break;
                            }
                        case 2: //left
                            {
                                previousSpot.x -= 1;
                                break;
                            }
                        case 3: //right
                            {
                                previousSpot.x += 1;
                                break;
                            }
                    }
                }
                else if ((!dashTriggered && !collision.collider.GetComponent<ignoreCombo>()))
                    gameCon.ResetCombo();

                nextSpot = previousSpot;
                previousSpot = transform.position;
            }
        }
        else if (collision.collider.tag == "Barrel")
        {
            sound.InvokePrioritySound(barrelDeath);
            playState = "dead";
            if (collision.collider.GetComponent<OneWayHazard>().axis == 'x')
                deathType = "rolledX";
            else
                deathType = "rolledY";
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if(collision.collider.tag == "BasicCollision")
        {
            if (!collision.collider.GetComponent<SwitchReceiverTimed>())
            {
                checkDeath += Time.deltaTime;
                if (checkDeath > 1 / ((float)gameCon.BPM / 60))
                {
                    sound.InvokePrioritySound(null);
                    playState = "dead";
                    deathType = "rolledX";
                    GetComponent<CircleCollider2D>().enabled = false;
                    spr.enabled = false;
                }
            }
            else
            {
                sound.InvokePrioritySound(null);
                playState = "dead";
                    deathType = "rolledX";
                    GetComponent<CircleCollider2D>().enabled = false;
                    spr.enabled = false;
            }
        }
    }

    //Player Control Checks
    //System Check for Game Controller
    void OnRoomReset()
    {
        gameCon.ResetRoom();
    }

    void OnQuitGame()
    {
        gameCon.LoadRoom("0 Title");
    }

    void OnGoNext()
    {
        if (playState == "lvClear")
        {
            string nextLv = FindObjectOfType<GoalTrigger>().nextRoom;
            //currScene = SceneManager.GetActiveScene();
            //nextScene = SceneManager.GetSceneAt(currScene.buildIndex + 1);
            gameCon.LoadRoom(nextLv);
        }
    }
    
    //Player Inputs
    void OnDash()
    {
        if (mainInput == "" || buffTime > 0) dashTriggered = true;
    }

    void OnUseItem()
    {
        if (mainInput == "" || buffTime > 0) itemState = true;
    }

    void OnMovLeft(InputValue val)
    {
        if (val.Get<float>() == 1) left = 1;
        else left = 0;
    }
    void OnMovRight(InputValue val)
    {
        if (val.Get<float>() == 1) right = 1;
        else right = 0;
    }
    void OnMovUp(InputValue val)
    {
        if (val.Get<float>() == 1) up = 1;
        else up = 0;
    }
    void OnMovDown(InputValue val)
    {
        if (val.Get<float>() == 1) down = 1;
        else down = 0;
    }

    // Update is called once per frame
    void Update()
    {
        //temporary for testing
        //itemHeld = true;

        moveSpeed = gameCon.GetMoveSpeed();
        bufferLen = (1 / ((float)gameCon.BPM / 60)) / 8; //buffer length is a 32nd note long

        //Input handling
        if (canMove && playState == "normal")
        {
            if (mainInput == "")
                moveCalls();
            else bufferState();
        }
        else if (mainInput != "")
        {
            executeAction();
        }
        else if (playState == "stuck")
        {
            Debug.Log("Stuck State called");
            stuckCalls();
        }
        
        if (playState == "dead")
        {
            GetComponent<PulseOnBeat>().enabled = false;
            hitSystem.Stop();
            //destroy any visual elements attached to player that isn't the player itself
            foreach (SpriteRenderer childObj in GetComponentsInChildren<SpriteRenderer>())
            {
                if(!childObj.GetComponent<PlayerMovement>())
                    Destroy(childObj.gameObject);
            }
            killPlayer(deathType);
        }

        if(playState == "victoryAnim")
        {
            hitSystem.Stop();
            //destroy any visual elements attached to player that isn't the player itself
            foreach (SpriteRenderer childObj in GetComponentsInChildren<SpriteRenderer>())
            {
                if (!childObj.GetComponent<PlayerMovement>())
                    Destroy(childObj.gameObject);
            }
        }

        if(playState == "centeringCamera")
        {
            
            Camera cam = FindObjectOfType<Camera>();
            if (transform.position.x == cam.transform.position.x && transform.position.y == cam.transform.position.y)
            {
                gameCon.StartLevel();
                playState = "normal";
                playerInputActions.ArenaMovement.Enable();
                //clear inputs that require instant pushes
                dashTriggered = false;
                itemState = false;
                //canMove = true;

            }
        }
    }

    void moveCalls()
    {

        playSound = true;
        collided = false;
        Debug.Log("moveCalls called");
        //Modifier controls
        if (dashTriggered)
        {
            mainInput = "dash";
            buffTime = bufferLen;
            previousSpot = transform.position;
            nextSpot = previousSpot;
            switch(facing)
            {
                case 0: nextSpot.y += travelDistance * 2; break;
                case 1: nextSpot.y -= travelDistance * 2; break;
                case 2: nextSpot.x -= travelDistance * 2; break;
                case 3: nextSpot.x += travelDistance * 2; break;
            }
        }
        else if(itemState) //&& itemHeld
        {
            mainInput = "item";
            buffTime = bufferLen;
        }
        //Movement controls
        else if (left > 0)
        {
            mainInput = "move";
            facing = 2; spr.sprite = sprLeft;
            buffTime = bufferLen;
            previousSpot = transform.position;
            nextSpot = previousSpot;
            nextSpot.x -= travelDistance;
            travelDirection = -1;
            //gameCon.AcceptMovement();
            //transform.Translate(new Vector3(-travelDistance * dashMod, 0, 0));
            //movement = StartCoroutine(movePlayer(1, travelDirection, moveSpeed, moveSound));
        }
        else if (right > 0)
        {
            mainInput = "move";
            facing = 3; spr.sprite = sprRight;
            buffTime = bufferLen;
            previousSpot = transform.position;
            nextSpot = previousSpot;
            nextSpot.x += travelDistance;
            travelDirection = 1;
            //gameCon.AcceptMovement();
            //transform.Translate(new Vector3(travelDistance * dashMod, 0, 0));
            //movement = StartCoroutine(movePlayer(1, travelDirection, moveSpeed, moveSound));
        }
        else if (up > 0)
        {
            mainInput = "move";
            facing = 0; spr.sprite = sprUp;
            buffTime = bufferLen;
            previousSpot = transform.position;
            nextSpot = previousSpot;
            nextSpot.y += travelDistance;
            travelDirection = 2;
            //gameCon.AcceptMovement();
            //transform.Translate(new Vector3(0, travelDistance * dashMod, 0));
            //movement = StartCoroutine(movePlayer(1, travelDirection, moveSpeed, moveSound));
        }
        else if (down > 0)
        {
            mainInput = "move";
            facing = 1; spr.sprite = sprDown;
            buffTime = bufferLen;
            previousSpot = transform.position;
            nextSpot = previousSpot;
            nextSpot.y -= travelDistance;
            travelDirection = -2;
            //gameCon.AcceptMovement();
            //transform.Translate(new Vector3(0, -travelDistance * dashMod, 0));
            //movement = StartCoroutine(movePlayer(1, travelDirection, moveSpeed, moveSound));
        }
    }

    void bufferState()
    {
        Debug.Log("bufferState called");
        if (buffTime > 0)
        {
            //check for combos
            switch(mainInput)
            {
                case "dash":
                {
                        if (left > 0)
                        {
                            travelDirection = -1;
                            facing = 2; spr.sprite = sprLeft;
                            sound.InvokePlayerSound(dashSound);
                            playSound = false;
                            gameCon.AcceptMovement();
                            gameCon.ComboPlus();
                            DetermineHitColor(); //calls particles
                            buffTime = 0;
                            nextSpot = previousSpot;
                            nextSpot.x -= travelDistance * 2;
                            canMove = false;
                        }
                        else if (right > 0)
                        {
                            travelDirection = 1;
                            facing = 3; spr.sprite = sprRight;
                            sound.InvokePlayerSound(dashSound);
                            playSound = false;
                            gameCon.AcceptMovement();
                            gameCon.ComboPlus();
                            DetermineHitColor(); //calls particles
                            buffTime = 0;
                            nextSpot = previousSpot;
                            nextSpot.x += travelDistance * 2;
                            canMove = false;
                        }
                        else if (up > 0)
                        {
                            travelDirection = 2;
                            facing = 0; spr.sprite = sprUp;
                            sound.InvokePlayerSound(dashSound);
                            playSound = false;
                            gameCon.AcceptMovement();
                            gameCon.ComboPlus();
                            DetermineHitColor(); //calls particles
                            buffTime = 0;
                            nextSpot = previousSpot;
                            nextSpot.y += travelDistance * 2;
                            canMove = false;
                        }
                        else if(down > 0)
                        {
                            travelDirection = -2;
                            facing = 1; spr.sprite = sprDown;
                            sound.InvokePlayerSound(dashSound);
                            playSound = false;
                            gameCon.AcceptMovement();
                            gameCon.ComboPlus();
                            DetermineHitColor(); //calls particles
                            buffTime = 0;
                            nextSpot = previousSpot;
                            nextSpot.y -= travelDistance * 2;
                            canMove = false;
                        }
                        
                    break;
                }
                case "move":
                {
                        if (dashTriggered == true)
                        {
                            mainInput = "dash";
                            break;
                        }
                        else if (itemState == true)
                        {
                            mainInput = "item";
                            break;
                        }
                    if(left == 1 && up == 1)
                    {
                            //left = 0; up = 0;
                            sound.InvokePlayerSound(moveSound);
                            playSound = false;
                            gameCon.AcceptMovement();
                        gameCon.ComboPlus();
                            DetermineHitColor(); //calls particles
                            facing = 0; spr.sprite = sprUp;
                        buffTime = 0;
                        nextSpot = previousSpot;
                        nextSpot.x -= travelDistance;
                        nextSpot.y += travelDistance;
                        canMove = false;
                    }
                    else if (left == 1 && down == 1)
                    {
                            //left = 0; down = 0;
                            sound.InvokePlayerSound(moveSound);
                            playSound = false;
                            gameCon.AcceptMovement();
                        gameCon.ComboPlus();
                            DetermineHitColor(); //calls particles
                            facing = 1; spr.sprite = sprDown;
                        buffTime = 0;
                        nextSpot = previousSpot;
                        nextSpot.x -= travelDistance;
                        nextSpot.y -= travelDistance;
                        canMove = false;
                    }
                    else if (right == 1 && up == 1)
                    {
                            //right = 0; up = 0;
                            sound.InvokePlayerSound(moveSound);
                            playSound = false;
                            gameCon.AcceptMovement();
                        gameCon.ComboPlus();
                            DetermineHitColor(); //calls particles
                            facing = 0; spr.sprite = sprUp;
                        buffTime = 0;
                        nextSpot = previousSpot;
                        nextSpot.x += travelDistance;
                        nextSpot.y += travelDistance;
                        canMove = false;
                    }
                    else if (right == 1 && down == 1)
                    {
                            //right = 0; down = 0;
                            sound.InvokePlayerSound(moveSound);
                            playSound = false;
                            gameCon.AcceptMovement();
                        gameCon.ComboPlus();
                            DetermineHitColor(); //calls particles
                            facing = 1; spr.sprite = sprDown;
                        buffTime = 0;
                        nextSpot = previousSpot;
                        nextSpot.x += travelDistance;
                        nextSpot.y -= travelDistance;
                        canMove = false;
                    }
                        break; 
                }
                case "item":
                    {
                        if (left > 0)
                        {
                            travelDirection = -1;
                            facing = 2; spr.sprite = sprLeft;
                            gameCon.AcceptMovement();
                            gameCon.ComboPlus();
                            DetermineHitColor(); //calls particles
                            buffTime = 0;
                            canMove = false;
                        }
                        else if (right > 0)
                        {
                            travelDirection = 1;
                            facing = 3; spr.sprite = sprRight;
                            gameCon.AcceptMovement();
                            gameCon.ComboPlus();
                            DetermineHitColor(); //calls particles
                            buffTime = 0;
                            canMove = false;
                        }
                        else if (up > 0)
                        {
                            travelDirection = 2;
                            facing = 0; spr.sprite = sprUp;
                            gameCon.AcceptMovement();
                            gameCon.ComboPlus();
                            DetermineHitColor(); //calls particles
                            buffTime = 0;
                            canMove = false;
                        }
                        else if (down > 0)
                        {
                            travelDirection = -2;
                            facing = 1; spr.sprite = sprDown;
                            gameCon.AcceptMovement();
                            gameCon.ComboPlus();
                            DetermineHitColor(); //calls particles
                            buffTime = 0;
                            canMove = false;
                        }

                        break;
                    }
            }
            buffTime -= Time.deltaTime;
        }
        else
        {
            //determine sound enqueuement
            if (playSound)
            {
                switch (mainInput)
                {
                    case "move":
                        {
                            sound.InvokePlayerSound(moveSound);
                            playSound = false;
                            DetermineHitColor(); //calls particles
                            break;
                        }
                    case "dash":
                        {
                            sound.InvokePlayerSound(dashSound);
                            playSound = false;
                            DetermineHitColor(); //calls particles
                            break;
                        }
                    case "item":
                        {
                            if (!itemHeld && itemState)
                            {
                                mainInput = "";
                                itemState = false;
                            }
                            break;
                        }
                }
            }
            gameCon.AcceptMovement();
            gameCon.ComboPlus();
            buffTime = 0;
            canMove = false;
        }
    }

    void executeAction()
    {
        Debug.Log("executeAction called with mainInput \'" + mainInput + "\'");

        //check for weird collision (only needed at high speeds)
        if(moveSpeed > 8)
        {
            if((nextSpot.x) % 1 != 0)
            {
                if (travelDirection > 0) //right -> change left
                {
                    nextSpot.x = Mathf.Floor(nextSpot.x);
                }
                else
                {
                    nextSpot.x = Mathf.Ceil(nextSpot.x);
                }
                Debug.Log("OOPS! Caught a slip that almost knocked the player off course");
            }
            if ((nextSpot.y) % 1 != 0)
            {
                if(travelDirection > 0) //up -> change down
                {
                    nextSpot.y = Mathf.Floor(nextSpot.y);
                }
                else
                {
                    nextSpot.y = Mathf.Ceil(nextSpot.y);
                }
                Debug.Log("OOPS! Caught a slip that almost knocked the player off course");
            }
        }

        switch (mainInput)
        {
            case "dash": goto case "move";
            case "move":
                {
                    //need to modify this to fit with BPM modifier
                    float movX = (nextSpot.x - previousSpot.x) * moveSpeed * Time.deltaTime;
                    float movY = (nextSpot.y - previousSpot.y) * moveSpeed * Time.deltaTime;

                    //should we keep moving or terminate?
                    //check for positive or negative movement (hor & ver)
                    //+x, +y, or both
                    if ((movX > 0 && movY == 0)
                    || (movY > 0 && movX == 0)
                    || (movX > 0 && movY > 0))
                    {

                        //catch when to end the lerp
                        if (transform.position.x > nextSpot.x || transform.position.y > nextSpot.y)
                        {
                            if (dashTriggered) dashTriggered = false;
                            transform.position = nextSpot;
                            mainInput = "";
                            GetComponent<PulseOnBeat>().amount = 0.3f;
                            //canMove = true;
                            //up = 0; down = 0; left = 0; right = 0;
                        }
                        //move player
                        else
                        {
                            transform.Translate(new Vector3(movX, movY, 0));
                        }
                    }
                    //-x, -y, or both
                    else if ((movX < 0 && movY == 0)
                        || (movY < 0 && movX == 0)
                        || (movX < 0 && movY < 0))
                    {
                        //catch when to end the lerp
                        if (transform.position.x < nextSpot.x || transform.position.y < nextSpot.y)
                        {
                            if (dashTriggered) dashTriggered = false;
                            transform.position = nextSpot;
                            mainInput = "";
                            GetComponent<PulseOnBeat>().amount = 0.3f;
                            //canMove = true;
                            //up = 0; down = 0; left = 0; right = 0;
                        }
                        //move player
                        else
                        {
                            //need to modify this to fit with BPM modifier
                            transform.Translate(new Vector3(movX, movY, 0));
                        }
                    }
                    //-x+y, +x-y
                    else
                    {
                        //catch when to end the lerp
                        if ((transform.position.x < nextSpot.x && transform.position.y > nextSpot.y && (movX < 0 && movY > 0))
                       || (transform.position.x > nextSpot.x && transform.position.y < nextSpot.y && (movX > 0 && movY < 0)))
                        {
                            transform.position = nextSpot;
                            mainInput = "";
                            GetComponent<PulseOnBeat>().amount = 0.3f;
                            // canMove = true;
                            //up = 0; down = 0; left = 0; right = 0;
                        }
                        //move player
                        else
                        {
                            transform.Translate(new Vector3(movX, movY, 0));
                        }
                    }
                    break;
                }
            case "item":
                {
                    switch (currItemID)
                    {
                        case 1: //rock
                            {
                                if (itemHeld)
                                {
                                    beatPerformed = gameCon.currBeatQtr;
                                    Vector3 spawnPos = transform.position;
                                    switch (facing)
                                    {
                                        case 0: //up
                                            {
                                                spawnPos.y += 0.5f;
                                                break;
                                            }
                                        case 1: //down
                                            {
                                                spawnPos.y -= 0.5f;
                                                break;
                                            }
                                        case 2: //left
                                            {
                                                spawnPos.x -= 0.5f;
                                                break;
                                            }
                                        case 3: //right
                                            {
                                                spawnPos.x += 0.5f;
                                                break;
                                            }
                                    }

                                    spawnedItem = Instantiate(itemList[currItemID], spawnPos, new Quaternion());
                                    itemHeld = false;
                                    itemState = false;
                                    gameCon.InsertItemUI(0);
                                }
                                if (spawnedItem == null || spawnedItem.GetComponent<ThrownRock>().readyToContinue)
                                {
                                    spawnedItem = null;
                                    currItemID = 0;
                                    if (gameCon.currBeatQtr != beatPerformed)
                                    {
                                        mainInput = "";
                                        canMove = true;
                                    }
                                }
                                else
                                {
                                    if (spawnedItem.GetComponent<ThrownRock>().movSpeed == 0)
                                    {
                                        spawnedItem.GetComponent<ThrownRock>().movSpeed = moveSpeed * 2;
                                        spawnedItem.GetComponent<ThrownRock>().movDir = facing;
                                    }
                                }
                                break;
                            }
                        case 2: //water
                            {
                                if (itemHeld)
                                {
                                    beatPerformed = gameCon.currBeatQtr;
                                    Vector3 spawnPos = transform.position;
                                    switch (facing)
                                    {
                                        case 0: //up
                                            {
                                                spawnPos.y += 0.6f;
                                                break;
                                            }
                                        case 1: //down
                                            {
                                                spawnPos.y -= 0.6f;
                                                break;
                                            }
                                        case 2: //left
                                            {
                                                spawnPos.x -= 0.6f;
                                                break;
                                            }
                                        case 3: //right
                                            {
                                                spawnPos.x += 0.6f;
                                                break;
                                            }
                                    }

                                    spawnedItem = Instantiate(itemList[currItemID], spawnPos, new Quaternion());
                                    itemHeld = false;
                                    itemState = false;
                                    gameCon.InsertItemUI(0);
                                }
                                if (spawnedItem == null || spawnedItem.GetComponent<ThrownWater>().readyToContinue)
                                {
                                    spawnedItem = null;
                                    currItemID = 0;
                                    if (gameCon.currBeatQtr != beatPerformed)
                                    {
                                        mainInput = "";
                                        canMove = true;
                                    }
                                }
                                else
                                {
                                    if (spawnedItem.GetComponent<ThrownWater>().facing < 0)
                                    {
                                        spawnedItem.GetComponent<ThrownWater>().facing = facing;
                                    }
                                }
                                break;
                            }
                        default: //catch irreg calls after item is unlinked from player
                            {
                                spawnedItem = null;
                                itemState = false;
                                currItemID = 0;
                                mainInput = "";
                                break;
                            }
                    }
                    //clear inputs that require instant pushes
                    dashTriggered = false;
                    itemState = false;
                    break;
                }
        }

        //failsafe for spot-turns
        itemState = false;
    }

    void stuckCalls()
    {
        GetComponent<PulseOnBeat>().amount = 0.09f;
        soundTone.clip = wallBump;
        nextSpot = transform.position;
        if (stickyTaps > 0)
        {
            if(mainInput == "" && canMove) moveCalls();
            left = 0; right = 0; up = 0; down = 0; dashTriggered = false;
            if (canMove && nextSpot != transform.position)
            {
                gameCon.AcceptMovement();
                gameCon.ComboPlus();
                mainInput = "";
                canMove = false;
                nextSpot = transform.position;
                stickyTaps -= 1;
                sound.InvokePlayerSound(wallBump);
            }
            
        }
        else
        {
            playState = "normal";
            canMove = false;
        }
    }

    void killPlayer(string protocol)
    {
        if (GetComponentInChildren<beatIndicator>())
            Destroy(GetComponentInChildren<beatIndicator>().gameObject);
        if (GetComponentInChildren<StickyTrapUI>())
            Destroy(GetComponentInChildren<StickyTrapUI>().gameObject);
        switch(protocol)
        {
            case "snapped":
                {
                    if (transform.localScale.x > 0.1f)
                    {
                        deathTimer = 1;
                        Vector3 playerScale = transform.localScale;
                        playerScale.x -= Time.deltaTime * moveSpeed;
                        transform.localScale = playerScale;
                    }
                    else
                    {
                        transform.localScale = new Vector3(0.1f,0.5f,0);
                        deathTimer -= Time.deltaTime;
                        if(deathTimer <= 0) playState = "terminate";
                    }
                    break;
                }
            case "rolledX":
                {
                    if (transform.localScale.x < 0.8)
                    {
                        deathTimer = 1;
                        Vector3 playerScale = transform.localScale;
                        playerScale.x += Time.deltaTime * moveSpeed;
                        transform.localScale = playerScale;
                    }
                    else
                    {
                        transform.localScale = new Vector3(0.8f, 0.5f, 0);
                        deathTimer -= Time.deltaTime;
                        if (deathTimer <= 0) playState = "terminate";
                    }
                    break;
                }
            case "rolledY":
                {
                    if (transform.localScale.y < 0.8)
                    {
                        deathTimer = 1;
                        Vector3 playerScale = transform.localScale;
                        playerScale.y += Time.deltaTime * moveSpeed;
                        transform.localScale = playerScale;
                    }
                    else
                    {
                        transform.localScale = new Vector3(0.5f, 0.8f, 0);
                        deathTimer -= Time.deltaTime;
                        if (deathTimer <= 0) playState = "terminate";
                    }
                    break;
                }
            case "pitfall":
                {
                    if (transform.localScale.x > 0 && transform.localScale.y > 0)
                    {
                        deathTimer = 0.7f;
                        Vector3 playerScale = transform.localScale;
                        playerScale.x -= Time.deltaTime;
                        playerScale.y -= Time.deltaTime;
                        transform.localScale = playerScale;
                    }
                    else
                    {
                        transform.localScale = Vector3.zero;
                        deathTimer -= Time.deltaTime;
                        if (deathTimer <= 0) playState = "terminate";
                    }
                    break;
                }
            case "fire":
                {
                    if (transform.localScale.x > .25 && transform.localScale.y > .25)
                    {
                        Color darken = spr.color;
                        deathTimer = 0.7f;
                        Vector3 playerScale = transform.localScale;
                        playerScale.x -= Time.deltaTime;
                        playerScale.y -= Time.deltaTime;
                        darken.b -= 3 * Time.deltaTime;
                        darken.g -= 3 * Time.deltaTime;
                        darken.r -= 3 * Time.deltaTime;
                        spr.color = darken;
                        transform.localScale = playerScale;
                    }
                    else
                    {
                        spr.color = Color.black;
                        deathTimer -= Time.deltaTime;
                        if (deathTimer <= 0) playState = "terminate";
                    }
                    break;
                }
            default:
                {
                    //disable movement
                    //play animation
                    break;
                }
        }
    }

    void DetermineHitColor()
    {
        Debug.Log("Beat is " + gameCon.GetBeatRemainingSixteenth());
        hitSystem.Stop();

        int check = (int)gameCon.GetBeatRemainingSixteenth();

        //critical hit is yellow
        if(check == 1)
        {
            ParticleSystem.MainModule main = hitSystem.main;
            main.startColor = new Color(255/255f,220/255f,0/255f);
        }
        else if(check != 3) 
        {
            ParticleSystem.MainModule main2 = hitSystem.main;
            main2.startColor = new Color(255/255f,92/255f,194/255f);
        }
        else
        {
            ParticleSystem.MainModule main3 = hitSystem.main;
            main3.startColor = new Color(200/255f, 200/255f, 200/255f);
        }
        hitSystem.Play();
    }




    /*movement coroutine
    //distance is a store of how far the player moves
    //travelDirection indicates which neighboring tile to move to
    //timeLen covers the length of time to travel
    //This is legacy and will be phased out for a new logic system
    private IEnumerator movePlayer(float distanceMod, int travelDirection, float timeLen, AudioClip soundByte)
    {
        soundTone.clip = soundByte;
        float initLocX = this.transform.position.x;
        float initLocY = this.transform.position.y;
        Vector3 distance = new Vector3();
        switch(travelDirection)
        {
            case -1: //left
                {
                    distance.x = travelDistance * -distanceMod;
                    bool soundTrip = false;
                    soundTone.Play();
                    float travelStep = (distance.x / timeLen) * Time.deltaTime;
                    {
                        while (this.transform.position.x > (initLocX + distance.x))
                        {
                            if (transform.position.x < (initLocX + (distance.x / 2)) && !soundTrip)
                            {

                                soundTone.Play();
                                soundTrip = true;
                            }
                            transform.Translate(new Vector3(travelStep, 0, 0));
                            yield return null;
                        }
                        //snap player to expected spot if calculations were off
                        if (this.transform.position.x != (initLocX + distance.x))
                        {
                            travelStep = (initLocX + distance.x) - this.transform.position.x;
                            transform.Translate(new Vector3(travelStep, 0, 0));
                        }
                        //if (!soundTone.isPlaying)
                        //soundTone.pitch = 1.1f;
                        
                    }
                    //Debug.Log(testResult);
                    break;
                }
            case 1: //right
                {
                    distance.x = travelDistance * distanceMod;
                    bool soundTrip = false;
                    float travelStep = (distance.x / timeLen) * Time.deltaTime;
                    while (this.transform.position.x < (initLocX + distance.x))
                    {
                        if (transform.position.x > (initLocX + (distance.x / 2)) && !soundTrip)
                        {
                            soundTone.Play();
                            soundTrip = true;
                        }
                        transform.Translate(new Vector3(travelStep, 0, 0));
                        yield return null;
                    }
                    //snap player to expected spot if calculations were off
                    if (this.transform.position.x != (initLocX + distance.x))
                    {
                        travelStep =  (initLocX + distance.x) - this.transform.position.x;
                        transform.Translate(new Vector3(travelStep, 0, 0));
                    }
                    //soundTone.pitch = 1.2f;
                    break;
                }
            case 2: //up
                {
                    distance.y = travelDistance * distanceMod;
                    bool soundTrip = false;
                    //soundTone.Play();
                    float travelStep = (distance.y / timeLen) * Time.deltaTime;
                    while (this.transform.position.y < (initLocY + distance.y))
                    {
                        if(transform.position.y > (initLocY + (distance.y/2)) && !soundTrip)
                        {
                            soundTone.Play();
                            soundTrip = true;
                        }
                    transform.Translate(new Vector3(0, travelStep, 0));
                        yield return null;
                    }
                    //snap player to expected spot if calculations were off
                    if (this.transform.position.y != (initLocY + distance.y))
                    {
                        travelStep = (initLocY + distance.y) - this.transform.position.y;
                        transform.Translate(new Vector3(0, travelStep, 0));
                        
                    }
                    break;
                }
            case -2: //down
                {
                    distance.y = travelDistance * -distanceMod;
                    bool soundTrip = false;
                    //soundTone.Play();
                    float travelStep = (distance.y / timeLen) * Time.deltaTime;
                    while (this.transform.position.y > (initLocY + distance.y))
                    {
                        if (transform.position.y < (initLocY + (distance.y / 2)) && !soundTrip)
                        {
                            soundTone.Play();
                            soundTrip = true;
                        }
                        transform.Translate(new Vector3(0, travelStep, 0));
                        yield return null;
                    }
                    //snap player to expected spot if calculations were off
                    if (this.transform.position.y != (initLocY + distance.y))
                    {
                        travelStep = (initLocY + distance.y) - this.transform.position.y;
                        transform.Translate(new Vector3(0, travelStep, 0));
                    }
                    break;
                }
            default: //exceptions etc
                {
                    break;
                }
        }
        //Debug.Log("Movement Step Complete");
        if (!goalHit)
        {
            canMove = true;
            isGrounded = true;
        }
        yield return "Movement Step Complete";
    }

    //falling coroutine
    //give the illusion of player falling by keeping them in place
    //and shrinking scale, then set their scale back to normal and
    //teleport them to a given spawn point
    private IEnumerator fallDown(int animLength)
    {
        GameObject[] respawn = GameObject.FindGameObjectsWithTag("Respawn");
        Vector3 spawnLoc = respawn[0].transform.position;
        Vector3 defaultScale = transform.localScale;
        float shrink = (defaultScale.x / animLength) * Time.deltaTime;
        

        while (transform.localScale.x > 0)
        {
            canMove = false;
            if (transform.localScale.x - shrink < 0)
                shrink = transform.localScale.x;
            //wait for player movement to finish before scale reduction
            //if (transform.position.x % 0.5 == 0 && transform.position.y % 0.5 == 0)
            {
                Vector3 newScale = new Vector3(transform.localScale.x - shrink, transform.localScale.y - shrink, 1.0f);
                transform.localScale = newScale;
            }
            yield return null;
        }

        //wait a moment before spawn in
        float timer = 1f;
        while(timer > 0)
        {
            timer -= Time.deltaTime;
            yield return null;
        }
        transform.SetPositionAndRotation(spawnLoc, transform.rotation);
        transform.localScale = defaultScale;
        canMove = true;

    }

    private IEnumerator queueMove(float distanceMod, int travelDirection, float timeLen, AudioClip soundByte)
    {
        bool temp = isGrounded;
        while(!canMove)
        {
            yield return null;
        }
        if (secondSource.clip != soundByte)
            secondSource.clip = soundByte;
        secondSource.Play();
        isGrounded = temp;
        movement = StartCoroutine(movePlayer(distanceMod, travelDirection, timeLen, empty));
    }*/
}
