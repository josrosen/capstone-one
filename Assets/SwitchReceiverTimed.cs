using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwitchReceiverTimed : MonoBehaviour
{
    public ColliderSwitchTimed masterKey;
    float fadeSpeed = 0.01f;
    public bool reverse;
    public bool exception = false;
    public bool hideColor = false;
    bool currState = true;

    SpriteRenderer spr;
    Color activeCol;

    // Start is called before the first frame update
    void Start()
    {
        spr = GetComponent<SpriteRenderer>();
        if(!hideColor)
        {
            Color temp = spr.color;
            temp.a = 0.9f;
            spr.color = temp;
        }
        activeCol = spr.color;

        if (masterKey == null || (reverse && !exception))
        {
            if(!hideColor)
            {
                Color temp = spr.color;
                temp.a = 0.2f;
                spr.color = temp;
            }
            else spr.color = Color.clear;
            GetComponent<BoxCollider2D>().enabled = false;
            currState = false;
        }
        else currState = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (masterKey != null && !exception)
        {
            if (!reverse)
            {
                if (masterKey.turnedOn && !currState)
                {
                    GetComponent<BoxCollider2D>().enabled = false;
                    float alpha = spr.color.a;
                    alpha -= fadeSpeed;
                    if (alpha <= 0.2)
                    {
                        alpha = 0.2f;
                        currState = true;
                    }
                    spr.color = new Color(spr.color.r, spr.color.g, spr.color.b, alpha);

                }
                else if (!masterKey.turnedOn && currState)
                {
                    GetComponent<BoxCollider2D>().enabled = true;
                    float alpha = spr.color.a;
                    alpha += fadeSpeed;
                    if (alpha >= 1)
                    {
                        alpha = .9f;
                        currState = false;
                    }
                    spr.color = new Color(spr.color.r, spr.color.g, spr.color.b, alpha);
                }
            }
            else
            {
                if (masterKey.turnedOn && !currState)
                {
                    GetComponent<BoxCollider2D>().enabled = true;
                    float alpha = spr.color.a;
                    alpha += fadeSpeed;
                    if (alpha >= 1)
                    {
                        alpha = .9f;
                        currState = true;
                    }
                    spr.color = new Color(activeCol.r, activeCol.g, activeCol.b, alpha);
                }
                else if (!masterKey.turnedOn && currState)
                {
                    GetComponent<BoxCollider2D>().enabled = false;
                    float alpha = spr.color.a;
                    alpha -= fadeSpeed;
                    if (alpha <= 0.2)
                    {
                        alpha = 0.2f;
                        currState = false;
                    }
                    spr.color = new Color(spr.color.r, spr.color.g, spr.color.b, alpha);
                }
            }

        }
    }
}
