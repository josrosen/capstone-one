using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OneWayHazard : MonoBehaviour
{
    public float movDistance = 1;
    public float movSpeed = 0.5f;
    public char axis;
    public int movDir = 1;
    public float timerLen = 0.5f;
    bool hit = false;
    bool movLerp = false;
    Vector3 prevPos;
    Vector3 nextPos;

    TimedLogic parent;
    public Animator animator;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        GetComponent<CircleCollider2D>().enabled = false;
        if ((collision.tag == "BasicCollision" || collision.tag == "Barrel") && !hit)
        {
            hit = true;
            if (axis == 'x')
            {
                prevPos.x += 0.4f * -movDir;
                /*if (movDir < 0) //switch left -> right
                {
                    prevPos.x += 0.4f;

                }
                else //switch right -> left
                {
                    prevPos.x -= 0.4f;
                }*/
            }
            else if (axis == 'y')
            {
                prevPos.y += 0.4f * -movDir;
            }
            nextPos = prevPos;
            prevPos = transform.position;
            movDir *= -1;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player")
            GetComponent<CircleCollider2D>().isTrigger = true;
        GetComponent<CircleCollider2D>().enabled = false;
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        if ((collision.collider.tag == "BasicCollision" || collision.collider.tag == "Barrel") && !hit)
        {
            hit = true;
            if (axis == 'x')
            {
                if (prevPos.x % 0.2 != 0)
                    prevPos.x = nextPos.x + 1.4f * -movDir;
                else
                    prevPos.x += 0.4f * -movDir;
                /*if (movDir < 0) //switch left -> right
                {
                    prevPos.x += 0.4f;

                }
                else //switch right -> left
                {
                    prevPos.x -= 0.4f;
                }*/
            }
            else if (axis == 'y')
            {
                if (prevPos.y % 0.2 != 0)
                    prevPos.y = nextPos.y + 1.4f * -movDir;
                else
                    prevPos.y += 0.4f * -movDir;
            }
            nextPos = prevPos;
            prevPos = transform.position;
            movDir *= -1;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        parent = this.GetComponent<TimedLogic>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (parent.canMove)
        {
            //animation flags
            if (!animator.GetBool("IsMoving"))
                animator.SetBool("IsMoving", true);
            if (animator.GetInteger("MovDir") != movDir)
                animator.SetInteger("MovDir", movDir);

            if (!movLerp)
            {
                movLerp = true;
                prevPos = transform.position;
                nextPos = prevPos;
                if(axis == 'x')
                {
                    nextPos.x += movDistance * movDir;
                    transform.Translate(movDir * movDistance * parent.moveSpeed * Time.deltaTime, 0, 0);
                }
                else if (axis == 'y')
                {
                    nextPos.y += movDistance * movDir;
                    transform.Translate(0, movDir * movDistance * parent.moveSpeed * Time.deltaTime, 0);
                }
                
            }
            else
            {
                if (movDir > 0)
                {
                    if (axis == 'x')
                    {
                        if (transform.position.x < nextPos.x)
                        {
                            transform.Translate(movDistance * parent.moveSpeed * Time.deltaTime, 0, 0);
                            if (!GetComponent<CircleCollider2D>().enabled)
                            {
                                hit = false;
                                GetComponent<CircleCollider2D>().enabled = true;
                            }
                        }
                        else 
                        {
                            transform.position = nextPos;
                            movLerp = false;
                            parent.canMove = false;
                            animator.SetBool("IsMoving", false);
                        }
                    }
                    else
                    {
                        if (transform.position.y < nextPos.y)
                        {
                            transform.Translate(0, movDistance * parent.moveSpeed * Time.deltaTime, 0);
                            if (!GetComponent<CircleCollider2D>().enabled)
                            {
                                hit = false;
                                GetComponent<CircleCollider2D>().enabled = true;
                            }
                        }
                        else
                        {
                            transform.position = nextPos;
                            movLerp = false;
                            parent.canMove = false;
                            animator.SetBool("IsMoving", false);
                        }
                    }
                }
                else
                {
                    if (axis == 'x')
                    {
                        if (transform.position.x > nextPos.x)
                        {
                            transform.Translate(-movDistance * parent.moveSpeed * Time.deltaTime, 0, 0);
                            if (!GetComponent<CircleCollider2D>().enabled)
                            {
                                hit = false;
                                GetComponent<CircleCollider2D>().enabled = true;
                            }
                        }
                        else
                        {
                            transform.position = nextPos;
                            movLerp = false;
                            parent.canMove = false;
                            animator.SetBool("IsMoving", false);
                        }
                    }
                    else if(axis == 'y')
                    {
                        if (transform.position.y > nextPos.y)
                        {
                            transform.Translate(0, -movDistance * parent.moveSpeed * Time.deltaTime, 0);
                            if (!GetComponent<CircleCollider2D>().enabled)
                            {
                                hit = false;
                                GetComponent<CircleCollider2D>().enabled = true;
                            }
                        }
                        else
                        {
                            transform.position = nextPos;
                            movLerp = false;
                            parent.canMove = false;
                            animator.SetBool("IsMoving", false);
                        }
                    }
                }
            }
        }
    }
}
