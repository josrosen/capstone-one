using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnapTrap : MonoBehaviour
{

    public AudioSource sound;
    public Sprite baseSprite;
    public Sprite triggeredSprite;
    public ParticleSystem particles;
    bool triggered = false;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!triggered && (collision.tag == "Player" || collision.tag == "BasicCollision"))
        {
            sound.Play();
            GetComponent<CircleCollider2D>().enabled = false;
            GetComponent<SpriteRenderer>().sprite = triggeredSprite;
            Vector3 curr = transform.position;
            curr.z -= 3;
            transform.position = curr;
            triggered = true;
            particles.Play();
            GetComponent<PulseOnBeat>().enabled = false;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        triggered = false;
        GetComponent<SpriteRenderer>().sprite = baseSprite;
    }
}
