using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostPadProperties : MonoBehaviour
{
    public Sprite deactivatedFrame;
    public Sprite acivatedFrame;
    public int direction;
    public BoostSwitchTrigger switchBlock; //= new BoostSwitchTrigger();
    private SpriteRenderer spr;
    private BoxCollider2D hitbox;
    private int sprState;

    // Start is called before the first frame update
    void Start()
    {
        spr = GetComponent<SpriteRenderer>();
        hitbox = GetComponent<BoxCollider2D>();
        sprState = 0;
        hitbox.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(!switchBlock || switchBlock.tripped && sprState == 1)
        {
            spr.sprite = acivatedFrame;
            hitbox.enabled = true;
            sprState = 2;
        }
        else
        {
            if (sprState == 0)
            {
                spr.sprite = deactivatedFrame;
                sprState = 1;
            }
        }
    }
}
