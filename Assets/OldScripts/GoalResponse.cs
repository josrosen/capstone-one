using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalResponse : MonoBehaviour
{
    private ParticleSystem goalParticles;
    private bool tripped;
    // Start is called before the first frame update
    void Start()
    {
        goalParticles = GetComponent<ParticleSystem>();
        tripped = false;
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!tripped)
        {
            if (collision.transform.position.x % 0.5 == 0 && collision.transform.position.y % 0.5 == 0)
            {
                tripped = true;
                goalParticles.Play();
                AudioSource[] sounds = GetComponents<AudioSource>();
                for (int i = 0; i < sounds.Length; i++)
                    sounds[i].Play();
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
