using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostSwitchTrigger : MonoBehaviour
{
    SpriteRenderer spr;
    public bool tripped = false;
    // Start is called before the first frame update
    void Start()
    {
        spr = GetComponent<SpriteRenderer>();
        spr.color = Color.gray;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!tripped)
        {
            if (collision.transform.position.x % 0.5 == 0 && collision.transform.position.y % 0.5 == 0)
            {
                tripped = true;
                spr.color = Color.green;
                GetComponent<AudioSource>().Play();
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!tripped)
        {
            if (collision.transform.position.x % 0.5 == 0 && collision.transform.position.y % 0.5 == 0)
            {
                tripped = true;
                spr.color = Color.green;
                GetComponent<AudioSource>().Play();
            }
        }
    }
}
