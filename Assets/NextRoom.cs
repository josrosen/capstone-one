using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextRoom : MonoBehaviour
{
    bool loadCommence = false;
    public string nextScene;
    private IEnumerator sceneLoader;

    // Start is called before the first frame update
    void Start()
    {
        if (nextScene == null)
            nextScene = SceneManager.GetSceneAt(0).name;
    }

    void OnGoNext()
    {
        if (!loadCommence)
        {
            loadCommence = true;
            sceneLoader = LoadingRoom(nextScene);
            StartCoroutine(sceneLoader);
        }
    }
    void OnQuitGame()
    {
        Application.Quit();
    }

    //Room Loading
    public void LoadRoom(string roomName)
    {
        /*if (FindObjectOfType<PlayerMovement>() != null)
            playerPos = FindObjectOfType<PlayerScript>().transform.position;*/
        //FindObjectOfType<EventSystem>().enabled = false;
        sceneLoader = LoadingRoom(roomName);
        StartCoroutine(sceneLoader);
    }
    private IEnumerator LoadingRoom(string roomName)
    {
        //DontDestroyOnLoad(gameObject);
        //DontDestroyOnLoad(overworldUI);
        SceneManager.LoadScene(roomName, LoadSceneMode.Single);
        AsyncOperation nScene = SceneManager.LoadSceneAsync(roomName, LoadSceneMode.Single);
        nScene.allowSceneActivation = false;
        while (nScene.progress < 0.9f)
        {
            yield return null;
        }

        nScene.allowSceneActivation = true;

        while (!nScene.isDone)
        {
            yield return null;
        }

        Scene theScene = SceneManager.GetSceneByName(roomName);
        if (theScene.IsValid())
        {
            //SceneManager.MoveGameObjectToScene(gameObject, nextScene);
            SceneManager.SetActiveScene(theScene);
        }
    }
}
