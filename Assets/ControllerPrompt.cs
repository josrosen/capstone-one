using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllerPrompt : MonoBehaviour
{
    public bool isMain = false;
    ScoreTracker config;
    // Start is called before the first frame update
    void Start()
    {
        config = FindObjectOfType<ScoreTracker>();
        if (!config || !config.GetInputType()) //set input type to keyboard
        {
            if(!isMain)
                this.gameObject.SetActive(false);
        }
        else
        {
            this.gameObject.SetActive(true);
            Debug.Log("Setting control type to controller");
        }
    }
}
