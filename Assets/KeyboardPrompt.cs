using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyboardPrompt : MonoBehaviour
{
    public bool isMain = false;
    ScoreTracker config;
    // Start is called before the first frame update
    void Start()
    {
        config = FindObjectOfType<ScoreTracker>();
        if(!config || !config.GetInputType()) //set input type to keyboard
        {
            Debug.Log("Setting control type to keyboard");
        }
        else
        {
            if(!isMain)
                this.gameObject.SetActive(false);
        }
    }
}
