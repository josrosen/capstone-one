using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ColliderSwitchTimed : MonoBehaviour
{
    public Sprite inactiveStatue;
    public Sprite activeStatue;
    public int numBeats;
    public bool turnedOn = false;
    public AudioSource switchSound;
    public AudioClip switchOn;
    public AudioClip switchOff;
    public SpriteRenderer clockUI;
    public TextMesh timer;
    public ColliderSwitchTimed[] radio;

    GameController beatController;
    PulseOnDemand pulser;
    int currBeat;
    int beatRmn;

    //timer scaling properties
    bool scaling = false;
    Vector3 normalPos = new Vector3(-0.025f, -0.15f, -1);
    Vector3 upscaledPos = new Vector3(-.1f, -.07f, -1);

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "BasicCollision" || collision.collider.tag == "Barrel" || (collision.collider.tag == "Player" && collision.collider.GetComponent<PlayerMovement>().dashTriggered))
        {
            switchSound.volume = FindObjectOfType<ScoreTracker>().GetMusVolume();
            if (turnedOn) //only play sound for statue that was hit
            {
                switchSound.clip = switchOff;
                switchSound.Play();
            }
            else
            {
                switchSound.clip = switchOn;
                switchSound.Play();
            }
            if (radio.Length >= 1) CallRadio();
            if (collision.collider.tag == "Player") collision.collider.GetComponent<PlayerMovement>().dashTriggered = false;            
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        currBeat = 0;
        beatRmn = 0;
        //timer.gameObject.SetActive(false);
        beatController = FindObjectOfType<GameController>();
        if (!turnedOn)
        {
            GetComponent<SpriteRenderer>().sprite = inactiveStatue;
            GetComponent<SpriteRenderer>().color = new Color(188, 139, 139); //BC8B8B
        }
        else
        {
            GetComponent<SpriteRenderer>().sprite = activeStatue;
            GetComponent<SpriteRenderer>().color = new Color(186, 186, 139); //BABC8B
        }
        if (radio.Length < 1)
        {
            radio.Initialize();
            radio[0] = this;
        }
        timer.text = numBeats.ToString();
        pulser = GetComponent<PulseOnDemand>();


    }

    // Update is called once per frame
    void Update()
    {
        if(currBeat != beatController.currBeatQtr)
        {
            if(turnedOn)
            {
                beatRmn -= 1;
                timer.text = beatRmn.ToString();
                pulser.SetPulse();
            }
            currBeat = beatController.currBeatQtr;
        }

        //turn off switch
        if(turnedOn && beatRmn <= 0)
        {
            turnedOn = false;
            beatRmn = 0;
            //timer.gameObject.SetActive(false);
            scaling = true;
            GetComponent<SpriteRenderer>().sprite = inactiveStatue;
            GetComponent<SpriteRenderer>().color = new Color(188, 139, 139); //BC8B8B
            switchSound.volume /= 2;
            switchSound.clip = switchOff;
            switchSound.Play();
            timer.text = numBeats.ToString();
            pulser.SetPulse();
        }

        if (scaling)
            ScaleTimer();
    }

    void ScaleTimer()
    {
        if (turnedOn == true && timer.gameObject.transform.localScale.x < 3)
        {
            timer.gameObject.transform.localScale += new Vector3(0.01f, 0.01f, 0);
        }
        else if (turnedOn == false && timer.gameObject.transform.localScale.x > 1)
        {
            timer.gameObject.transform.localScale -= new Vector3(0.01f, 0.01f, 0);
        }
        else
        {
            if (turnedOn)
            {
                timer.gameObject.transform.localScale = new Vector3(3, 3, 0);
            }
            else
            {
                timer.gameObject.transform.localScale = Vector3.one;
                timer.gameObject.transform.localPosition = normalPos;
            }
            scaling = false;
        }
    }

    void CallRadio()
    {
        for(int i = 0; i < radio.Length; i++)
        {
            radio[i].AcceptCall();
        }
    }

    void AcceptCall()
    {
        if (!turnedOn)
        {
            turnedOn = true;
            beatRmn = numBeats;
            timer.text = beatRmn.ToString();
            scaling = true;
            timer.gameObject.transform.localPosition = upscaledPos;
            GetComponent<SpriteRenderer>().sprite = activeStatue;
            GetComponent<SpriteRenderer>().color = new Color(186, 186, 139); //BABC8B
            pulser.SetPulse();
            
        }
        else
        {
            turnedOn = false;
            beatRmn = 0;
            scaling = true;
            GetComponent<SpriteRenderer>().sprite = inactiveStatue;
            GetComponent<SpriteRenderer>().color = new Color(188, 139, 139); //BC8B8B            
            timer.text = numBeats.ToString();
            pulser.SetPulse();
        }
    }
}
