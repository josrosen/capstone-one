using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class DisplayEndResults : MonoBehaviour
{
    ScoreTracker resObj;
    public TextMeshProUGUI[] gradeTexts;
    // Start is called before the first frame update
    void Start()
    {
        resObj = FindObjectOfType<ScoreTracker>();
        if(resObj == null)
        {
            for (int i = 0; i < gradeTexts.Length; i++)
            {
                gradeTexts[i].text = "ERR";
                gradeTexts[i].color = Color.red;
            }
        }
        else
        {
            for (int i = 0; i < gradeTexts.Length; i++)
            {
                char result = resObj.ScoreArray[i];
                gradeTexts[i].text = result.ToString();
                
                switch(result)
                {
                    case 'S':
                        {
                            VertexGradient grad = gradeTexts[i].colorGradient;
                            grad.bottomLeft = new Color(1, 251 / 255f, 0); //yellow
                            grad.topRight = new Color(1, 251 / 255f, 0);
                            grad.topLeft = new Color(1, 121 / 255f, 0); //orange
                            grad.bottomRight = new Color(1, 121 / 255f, 0);
                            gradeTexts[i].colorGradient = grad;
                            break;
                        }
                    case 'A':
                        {
                            VertexGradient grad = gradeTexts[i].colorGradient;
                            grad.bottomLeft = new Color(125 / 255f, 1, 0); //light green
                            grad.topRight = new Color(125 / 255f, 1, 0);
                            grad.topLeft = new Color(7 / 255f, 32 / 255f, 23 / 255f); //blackish blue
                            grad.bottomRight = new Color(7 / 255f, 32 / 255f, 23 / 255f);
                            gradeTexts[i].colorGradient = grad;
                            break;
                        }
                    case 'B':
                        {
                            VertexGradient grad = gradeTexts[i].colorGradient;
                            grad.bottomLeft = new Color(0, 67 / 255f, 1); //blue
                            grad.topRight = new Color(0, 67 / 255f, 1);
                            grad.topLeft = new Color(70 / 255f, 0, 100 / 255f); //purple
                            grad.bottomRight = new Color(70 / 255f, 0, 100 / 255f);
                            gradeTexts[i].colorGradient = grad;
                            break;
                        }
                    case 'C':
                        {
                            VertexGradient grad = gradeTexts[i].colorGradient;
                            grad.bottomLeft = new Color(100 / 255f, 100 / 255f, 100 / 255f); //gray
                            grad.topRight = new Color(100 / 255f, 100 / 255f, 100 / 255f);
                            grad.topLeft = Color.white; //orange
                            grad.bottomRight = Color.white;
                            gradeTexts[i].colorGradient = grad;
                            break;
                        }
                    case 'F':
                        {
                            gradeTexts[i].color = Color.red;
                            break;
                        }
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
