using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class beatIndicator : MonoBehaviour
{
    Vector3 defaultSize;
    float currSixteenth;
    double beatLen;
    int reducing;

    GameController gameCon;
    SpriteRenderer spr;

    // Start is called before the first frame update
    void Start()
    {
        spr = GetComponent<SpriteRenderer>();
        gameCon = FindObjectOfType<GameController>();
        reducing = 0;
        currSixteenth = 0;
        defaultSize = transform.localScale;
        spr.color = Color.black;
    }

    // Update is called once per frame
    void Update()
    {
        //begin a lerp to shrink halfway 
        if(currSixteenth != gameCon.GetBeatRemainingSixteenth())
        {
            currSixteenth = gameCon.GetBeatRemainingSixteenth();
            //do we switch states?
            if (currSixteenth == 2)
                reducing = 2;
            else if (currSixteenth == 0)
                reducing = 1;
        }

        if(reducing == 0) //begin to reduce
        {
            float delta = transform.localScale.x * Time.deltaTime * gameCon.BPM/60;
            float reduc = transform.localScale.x - delta;
            if (reduc > 0)
                transform.localScale = new Vector3(reduc, reduc, 1);
            else
                transform.localScale = Vector3.zero;
            spr.color = new Color(spr.color.r + delta, spr.color.g + Time.deltaTime, spr.color.b + Time.deltaTime);

        }
        else if (reducing == 1)//what to do while waiting for beat to reset
        {
            transform.localScale = Vector3.zero;
            //transform.localScale = Vector3.zero;
        }
        else //reset and begin to reduce again
        {
            transform.localScale = defaultSize;
            spr.color = Color.black;
            reducing = 0;
        }
    }
}
