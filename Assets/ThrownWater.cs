using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrownWater : MonoBehaviour
{
    public float movSpeed = 0;
    float timer = 0.2f;
    public int facing = -1;
    bool notFaced = true;
    public bool readyToContinue = false;
    public AudioSource sound;

    /*private void OnCollisionEnter2D(Collision2D collision)
    {
        //play a hit sound then destruct
        soundPlayed = true;
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<CircleCollider2D>().enabled = false;
        totalDist = 2;
        //collisionSound.Play();
    }*/

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //play a hit sound then destruct
        if (collision.tag == "Fire")
        {
            collision.GetComponent<TriggerDestroy>().DestroyObject(true);
        }
        if(collision.tag == "StickyTrap")
        {
            collision.GetComponent<TriggerDestroy>().DestroyObject(false);
        }
    }

    /*
     * TODO FROM HERE
     * Our water is failing to collide with the desired objects, or the script is not
     * properly commencing. We need to find a way to fix this
     */


    private void Start()
    {
        movSpeed = 0;
        sound.volume = FindObjectOfType<ScoreTracker>().GetMusVolume();
        sound.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if(notFaced && facing >= 0)
        {
            switch(facing)
            {
                case 0: //up
                    {
                        transform.Rotate(new Vector3(0, 0, 90));
                        break;
                    }
                case 1: //down
                    {
                        transform.Rotate(new Vector3(0, 0, -90));
                        break;
                    }
                case 2: //left
                    {
                        transform.Rotate(new Vector3(0, 0, 180));
                        break;
                    }
                case 3: //right
                    {
                        GetComponent<SpriteRenderer>().flipX = false;
                        break;
                    }
            }
            notFaced = false;
        }
        //movement
        if (timer > 0)
        {
            timer -= Time.deltaTime;
        }
        else
        {
            if (!readyToContinue)
            {
                GetComponent<SpriteRenderer>().enabled = false;
                GetComponent<BoxCollider2D>().enabled = false;
                readyToContinue = true;
            }
            if(!sound.isPlaying)
                Destroy(gameObject);
        }
    }
}
