using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderSwitch : MonoBehaviour
{
    public bool turnedOn = false;
    public Sprite inactiveSpr;
    public Sprite activeSpr;
    public AudioSource switchSound;
    public AudioClip switchOn;
    public AudioClip switchOff;
    PulseOnDemand pulser;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.tag == "BasicCollision" || collision.collider.tag == "Barrel"|| (collision.collider.tag == "Player" && collision.collider.GetComponent<PlayerMovement>().dashTriggered))
        {
            if(!turnedOn)
            {
                turnedOn = true;
                GetComponent<SpriteRenderer>().sprite = activeSpr;
                GetComponent<SpriteRenderer>().color = new Color(186, 186, 139);
                switchSound.clip = switchOn;
                switchSound.Play();
                pulser.SetPulse();
            }
            else
            {
                turnedOn = false;
                GetComponent<SpriteRenderer>().sprite = inactiveSpr;
                GetComponent<SpriteRenderer>().color = new Color(188, 139, 139);
                switchSound.clip = switchOff;
                switchSound.Play();
                pulser.SetPulse();
            }
            if (collision.collider.tag == "Player") collision.collider.GetComponent<PlayerMovement>().dashTriggered = false;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (!turnedOn)
        {
            GetComponent<SpriteRenderer>().color = new Color(188, 139, 139); //BC8B8B
            GetComponent<SpriteRenderer>().sprite = inactiveSpr;
        }
        else
        {
            GetComponent<SpriteRenderer>().color = new Color(186, 186, 139); //BABC8B
            GetComponent<SpriteRenderer>().sprite = activeSpr;
        }
        pulser = GetComponent<PulseOnDemand>();
    }
}
