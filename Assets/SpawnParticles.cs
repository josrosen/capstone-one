using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnParticles : MonoBehaviour
{
    public ParticleSystem particles;

    public void callParticles()
    {
        ParticleSystem newObj = Instantiate(particles);
        newObj.transform.position = gameObject.transform.position;
    }
}
