using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class delayChecker : MonoBehaviour
{
    public AudioClip beatSound;
    public AudioSource soundPlayer;

    //Rhythm variables
    double levelStartTime = 0;
    float latestBeat;
    public float BPM = 100;
    float BPS;
    public double beatLen;
    public double beatTimer = 0;
    public float delay;

    public int currBeatQtr = 0;
    public int currBeatEighth = 0;
    int currBeatSixteenth = 0;

    // Start is called before the first frame update
    void Start()
    {
        BPS = BPM / 60f;
        beatLen = 60f / BPM;
    }

    // Update is called once per frame
    void Update()
    {
        if (beatTimer < beatLen)
        {
            beatTimer += Time.deltaTime;
            /*
            if (!comboGuard)
            {
                if (beatTimer > (beatLen * 0.6) && playerObj.canMove && playerObj.buffTime <= 0 && playerObj.playState == "normal")
                {
                    ResetCombo();
                    //playerObj.canMove = false;
                }
            }
            int temp = currBeatEighth;
            if (temp < GetBeatRemainingEighth())
            {
                anthrSoundPlayer.Play();
            }
            temp = currBeatSixteenth;
            if (temp < GetBeatRemainingSixteenth())
                oneMoreSoundPlayer.Play();*/
        }
        else
        {
            beatTimer = (beatTimer - beatLen) + Time.deltaTime; //accounting for the extra difference from deltaTime
            latestBeat = Time.realtimeSinceStartup;
            soundPlayer.Play();
            currBeatQtr = currBeatQtr < 3 ? currBeatQtr + 1 : 0;
            currBeatEighth = 0;
            currBeatSixteenth = 0;            
        }
    }

    //returns difference between beat and key strike in milliseconds
    public float AcceptInput()
    {
        float temp = Time.realtimeSinceStartup - latestBeat;
        if (temp > 0.5) //hitting early vs hitting late
            temp *= -1;
        return temp*1000;
    }

    public float GetBeatRemainingQuarter()
    {
        //Debug.Log("Quarter = " + currBeatQtr + "/4");
        return currBeatQtr;
    }

    public int GetBeatRemainingEighth()
    {
        double timeMath = beatTimer + delay;
        currBeatEighth = (timeMath > beatLen / 2) ? 1 : 0;
        currBeatEighth += currBeatQtr * 2;
        //Debug.Log("Eighth = " + currBeatEighth + "/8");
        return currBeatEighth;
    }

    public float GetBeatRemainingSixteenth()
    {
        double timeMath = beatTimer + delay;
        double temp = beatLen / 4;
        if (timeMath < temp)
            currBeatSixteenth = 0;
        else if (timeMath > temp && timeMath < temp * 2)
            currBeatSixteenth = 1;
        else if (timeMath > temp * 2 && timeMath < temp * 3)
            currBeatSixteenth = 2;
        else currBeatSixteenth = 3;
        currBeatSixteenth += currBeatEighth * 2;
        //Debug.Log("Sixteenth = " + currBeatSixteenth + "/16");
        return currBeatSixteenth;
    }
}
