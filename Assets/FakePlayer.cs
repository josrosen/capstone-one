using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakePlayer : MonoBehaviour
{
    public string[] actionList;
    public int[] actionMod;
    TimedLogic parent;
    int moveIndex = 0;

    //movement variables
    float moveSpeed;
    Vector3 nextSpot;
    Vector3 previousSpot;
    float travelDistance = 1;
    int travelDirection = 0;

    //animation variables
    int facing = 1;
    SpriteRenderer spr;
    public Sprite sprLeft;
    public Sprite sprRight;
    public Sprite sprUp;
    public Sprite sprDown;

    // Start is called before the first frame update
    void Start()
    {
        parent = GetComponent<TimedLogic>();
        spr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        //execute actions
        if(parent.canMove)
        {
            moveSpeed = parent.moveSpeed;


            switch(actionList[moveIndex])
            {
                case "left":
                    {
                        facing = 2; spr.sprite = sprLeft;
                        if (actionMod[moveIndex] == 0)
                        {
                            previousSpot = transform.position;
                            nextSpot = previousSpot + new Vector3(-1, 0, 0);
                            travelDirection = -1;
                            actionList[moveIndex] = "move";
                            goto case "move";
                        }
                        if(actionMod[moveIndex] == 2)
                        {
                            previousSpot = transform.position;
                            nextSpot = previousSpot + new Vector3(-1, -1, 0);
                            facing = 1; spr.sprite = sprDown;
                            travelDirection = -1;
                            actionList[moveIndex] = "move";
                            goto case "move";
                        }
                        EndAction();
                        break;
                    }
                case "right":
                    {
                        facing = 3; spr.sprite = sprRight;
                        if (actionMod[moveIndex] == 0)
                        {
                            previousSpot = transform.position;
                            nextSpot = previousSpot + new Vector3(1, 0, 0);
                            travelDirection = 1;
                            actionList[moveIndex] = "move";
                            goto case "move";
                        }
                        EndAction();
                        break;
                    }
                case "up":
                    {
                        facing = 0; spr.sprite = sprUp;
                        if (actionMod[moveIndex] == 0)
                        {
                            previousSpot = transform.position;
                            nextSpot = previousSpot + new Vector3(0, 1, 0);
                            travelDirection = 2;
                            actionList[moveIndex] = "move";
                            goto case "move";
                        }
                        EndAction();
                        break;
                    }
                case "down":
                    {
                        facing = 1; spr.sprite = sprDown;
                        if (actionMod[moveIndex] == 0)
                        {
                            previousSpot = transform.position;
                            nextSpot = previousSpot + new Vector3(0, -1, 0);
                            travelDirection = -2;
                            actionList[moveIndex] = "move";
                            goto case "move";
                        }
                        if (actionMod[moveIndex] == 2)
                            goto case "left";
                        EndAction();
                        break;
                    }
                case "dash":
                    {
                        previousSpot = transform.position;
                        nextSpot = previousSpot;
                        switch (facing)
                        {
                            case 0: nextSpot.y += travelDistance * 2; break;
                            case 1: nextSpot.y -= travelDistance * 2; break;
                            case 2: nextSpot.x -= travelDistance * 2; break;
                            case 3: nextSpot.x += travelDistance * 2; break;
                        }
                        actionList[moveIndex] = "move";
                        goto case "move";
                    }
                case "move":
                    {
                        //need to modify this to fit with BPM modifier
                        float movX = (nextSpot.x - previousSpot.x) * moveSpeed * Time.deltaTime;
                        float movY = (nextSpot.y - previousSpot.y) * moveSpeed * Time.deltaTime;

                        //should we keep moving or terminate?
                        //check for positive or negative movement (hor & ver)
                        //+x, +y, or both
                        if ((movX > 0 && movY == 0)
                        || (movY > 0 && movX == 0)
                        || (movX > 0 && movY > 0))
                        {

                            //catch when to end the lerp
                            if (transform.position.x > nextSpot.x || transform.position.y > nextSpot.y)
                            {
                                transform.position = nextSpot;
                                GetComponent<PulseOnBeat>().amount = 0.3f;
                                EndAction();
                            }
                            //move player
                            else
                            {
                                transform.Translate(new Vector3(movX, movY, 0));
                            }
                        }
                        //-x, -y, or both
                        else if ((movX < 0 && movY == 0)
                            || (movY < 0 && movX == 0)
                            || (movX < 0 && movY < 0))
                        {
                            //catch when to end the lerp
                            if (transform.position.x < nextSpot.x || transform.position.y < nextSpot.y)
                            {
                                transform.position = nextSpot;
                                GetComponent<PulseOnBeat>().amount = 0.3f;
                                EndAction();
                            }
                            //move player
                            else
                            {
                                //need to modify this to fit with BPM modifier
                                transform.Translate(new Vector3(movX, movY, 0));
                            }
                        }
                        //-x+y, +x-y
                        else
                        {
                            //catch when to end the lerp
                            if ((transform.position.x < nextSpot.x && transform.position.y > nextSpot.y && (movX < 0 && movY > 0))
                           || (transform.position.x > nextSpot.x && transform.position.y < nextSpot.y && (movX > 0 && movY < 0)))
                            {
                                transform.position = nextSpot;
                                GetComponent<PulseOnBeat>().amount = 0.3f;
                                EndAction();
                            }
                            //move player
                            else
                            {
                                transform.Translate(new Vector3(movX, movY, 0));
                            }
                        }
                        break;
                    }
            }
        }
    }

    void EndAction()
    {
        moveIndex++;
        //catch to destroy the player if sequence is complete
        if (moveIndex >= actionList.Length)
            Destroy(this.gameObject);
        parent.canMove = false;
    }
}
