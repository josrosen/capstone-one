using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class CameraFollow : MonoBehaviour
{
    int previewMode;
    public float speed = .008f;
    public float xLowBound;
    public float xHighBound;
    public float yLowBound;
    public float yHighBound;

    bool lockLeft = false;
    bool lockRight = false;
    bool lockUp = false;
    bool lockDown = false;

    Vector3 startingPan;
    PlayerMovement pTransform;
    PlayerInput playerInput;
    InGameControls playerInputActions;


    // Start is called before the first frame update
    void Start()
    {
        playerInput = GetComponent<PlayerInput>();
        playerInputActions = new InGameControls();
        playerInputActions.Camera.Enable();
        previewMode = 1;
        pTransform = FindObjectOfType<PlayerMovement>();
    }

    // Update is called once per frame
    void Update()
    {
        if (previewMode == 0)
        {
            transform.position = pTransform.transform.position + new Vector3(0, 0, -10);
        }
        else if(previewMode == 1)
        {
            if(playerInputActions.Camera.Proceed.WasReleasedThisFrame())
            {
                playerInputActions.Camera.Disable();
                pTransform.playState = "centeringCamera";
                previewMode = 2;
                startingPan = transform.position;
                GetComponent<AudioSource>().Play();
                FindObjectOfType<SoundPlayer>().StopBGM();
            }

            Vector2 inputVec = playerInputActions.Camera.Movement.ReadValue<Vector2>();
            inputVec *= speed;
            if (transform.position.x + inputVec.x < xLowBound)
            {
                inputVec.x = 0;
                lockLeft = true;
            }
            else if (transform.position.x - speed <= xLowBound)
                lockLeft = true;
            else
                lockLeft = false;
            if (transform.position.x + inputVec.x > xHighBound)
            {
                inputVec.x = 0;
                lockRight = true;
            }
            else if (transform.position.x + speed >= xHighBound)
                lockRight = true;
            else
                lockRight = false;
            if (transform.position.y + inputVec.y < yLowBound)
            {
                inputVec.y = 0;
                lockDown = true;
            }
            else if (transform.position.y - speed <= yLowBound)
                lockDown = true;
            else
                lockDown = false;
            if (transform.position.y + inputVec.y > yHighBound)
            {
                inputVec.y = 0;
                lockUp = true;
            }
            else if (transform.position.y + speed >= yHighBound)
                lockUp = true;
            else
                lockUp = false;

            transform.Translate(inputVec.x, inputVec.y, 0);

            //send signals to canvas
            if (lockLeft)
                FindObjectOfType<GenerateUI>().HideArrow("left");
            else FindObjectOfType<GenerateUI>().ShowArrow("left");
            if (lockRight)
                FindObjectOfType<GenerateUI>().HideArrow("right");
            else FindObjectOfType<GenerateUI>().ShowArrow("right");
            if (lockUp)
                FindObjectOfType<GenerateUI>().HideArrow("up");
            else FindObjectOfType<GenerateUI>().ShowArrow("up");
            if (lockDown)
                FindObjectOfType<GenerateUI>().HideArrow("down");
            else FindObjectOfType<GenerateUI>().ShowArrow("down");
        }
    }

    private void FixedUpdate()
    {
        if(previewMode == 2)
        {
            Vector3 translator = transform.position - pTransform.transform.position;
            translator.z = 0;
            if (Mathf.Abs(translator.x) < .1 && Mathf.Abs(translator.y) < .1)
            {
                transform.position = pTransform.transform.position;
                previewMode = 0;
            }
            else if (Mathf.Abs(translator.x) < 3 && Mathf.Abs(translator.y) < 3)
            {
                
                transform.Translate(translator * -3 * Time.deltaTime);
            
            }
            else
                transform.Translate(translator * -5 * Time.deltaTime);
        }
    }
}
